using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Domain.Abstract
{
    public interface ISubjectService
    {
        IQueryable<Subject> Subjects {get;}

        User Teacher { get; set; }
        /// <summary>
        /// Adds the subject to the repository
        /// </summary>
        /// <param name="subject"></param>
        /// <returns>Id of the added subject</returns>
        int CreateSubject(Subject subject);
        /// <summary>
        /// Changes the subject
        /// </summary>
        /// <param name="subject"></param>
        void ChangeSubject(Subject subject);
        /// <summary>
        /// Changes the subject's course number // TODO: should link IUserRepository to solve conflicts??? 
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="newCourseNumber"></param>

        void DeleteSubject(Subject subject);

        Task<Subject> GetSubjectByIdAsync(int subjectId);

        int AddHometask(Subject subject,
            Hometask hometask);

        bool ChangeHometask(Hometask toHometask);
        bool DeleteHometask(Hometask toHometask);
    }
}