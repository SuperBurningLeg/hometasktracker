﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Domain.Abstract
{
    public interface ISubjectSubscriber
    {
        Task SubscribeToSubject(int subjectId,
            string userId);

        Task ConfirmSubscription(int subjectId,
            string userId);

        Task UnsubscribeFromSubject(int subjectId,
            string userId);

        Task<IEnumerable<StudentSubject>> GetPending(int subjectId);

        Task AcceptStudent(int subjectId,
            string studentId);
    }
}
