using System.Linq;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Domain.Abstract
{
    public interface IUsersRepository
    {
        /// <summary>
        /// Users repository
        /// </summary>
        /// <returns></returns>
        IQueryable<User> Users {get;}
        /// <summary>Creates a User in the repository and returns it's ID</summary> <summary> 
        /// <param name="student"></param>
        /// <returns>Id of created student</returns>
        int AddStudent(User student);
        ///<summary>Creates a User in the repository and returns it's ID</summary>
        /// <param name="teacher"></param>
        /// <returns>Id of created teacher</returns>
        int AddTeacher(User teacher);
        ///<summary>Changes users password</summary>
        /// <param name="user"></param>
        /// <param name="newPassword">New password of the user</param>
        void ChangeUserPassword(User user, string newPassword);

        

        
    }
}