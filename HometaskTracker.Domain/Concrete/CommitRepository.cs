﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Query.Expressions;

namespace HometaskTracker.Domain.Concrete
{
    public class CommitRepository
    {
        private readonly EfDbContext _context;
        private readonly ISubjectService _subjectRepository;
        public User User { get; set; }

        public CommitRepository(EfDbContext context, ISubjectService subjectRepository)
        {
            _context = context;
            _subjectRepository = subjectRepository;
        }

        public async Task AddCommit(Commit commit, IEnumerable<CommitChange> changes, string repositoryName)
        {
            commit.Id = 0;
            _context.Commits.Add(commit);
            await _context.SaveChangesAsync();
            foreach (var change in changes)
            {
                change.CommitId = commit.Id;
                change.StudentId = User.Id;
                foreach (var ss in User.StudentSubjects)
                {
                    if (ss.RepositoryName != repositoryName)
                        continue;
                    var path = new PathString(change.Path);
                    var hasHometasks = ss.Subject.Hometasks.Any(h => path.StartsWithSegments(new PathString(h.RepositoryPath)));
                    if (hasHometasks)
                    {
                        Debug.Assert(ss.SubjectId != null, "ss.SubjectId != null");
                        change.SubjectId = ss.SubjectId.Value;
                        break;
                    }
                }
                _context.Changes.Add(change);
            }

            await _context.SaveChangesAsync();
        }

        public IEnumerable<Commit> GetCommits(string studentId,
            int hometaskId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == studentId);
            if(user == null)
                throw new ArgumentException("There is no users with that id");
            var hometask = _context.Hometasks.FirstOrDefault(h => h.Id == hometaskId);
            if (hometask == null)
                throw new ArgumentException("There is no hometasks with that id");

            var commits =
                _context.Commits.Include(c => c.Changes).Include(c => c.Marks)
                .Where(c => c.Changes.Any(cs =>
                   cs.Path.StartsWith(hometask.RepositoryPath)
                    && cs.StudentId == studentId && cs.SubjectId == hometask.SubjectId
                ));

            return commits;
        }

        public IEnumerable<double> GetTopMarksCommits(string studentId,
            int subjectId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == studentId);
            if (user == null)
                throw new ArgumentException("There is no users with that id");
            var studentSubject = _context.StudentSubject
                .Include(ss => ss.Subject).ThenInclude(s => s.Hometasks)
                .Include(ss => ss.CommitChanges).ThenInclude(cc => cc.Commit).ThenInclude(c => c.Marks)
                .FirstOrDefault(s => s.SubjectId == subjectId && s.StudentId == studentId);
            if (studentSubject == null)
                throw new ArgumentException("There is no subject with that id");

            var ms = studentSubject.Subject.Hometasks.SelectMany(h =>
                studentSubject.CommitChanges.Where(cc => cc.Path.StartsWith(h.RepositoryPath))
                    .Select(cc => new
                    {
                        Mark = cc.Commit.Marks.Where(m => m.HometaskId == h.Id &&
                                                          m.StudentId == studentId)
                            .DefaultIfEmpty(new Mark {Value = 0, HometaskId = h.Id}
                    ).OrderBy(m => m.HometaskId).Max(m => m.Value),
                    HometaskId = h.Id
                    }));
            
            var marks = new List<double>();
            foreach (var subjectHometask in studentSubject.Subject.Hometasks)
            {
                var hMark = ms.FirstOrDefault(m => m.HometaskId == subjectHometask.Id);
                marks.Add( hMark?.Mark ?? 0);
            }
            return marks;
        }

        public IEnumerable<HometaskCommit> GetCommits(int subjectId)
        {
            var changes = GetCommitChangesWithoutMark();
            return changes.Where(cs => cs.SubjectId == subjectId).Select(change => new HometaskCommit
            {
                Commit = change.Commit,
                Hometask = change.StudentSubject.Subject.Hometasks.Single(h => change.Path.StartsWith(h.RepositoryPath)),
                Student = change.StudentSubject.Student,
                Subject = change.StudentSubject.Subject
            });
        }

        public IEnumerable<HometaskCommit> GetLastCommits(int subjectId)
        {
            int count = 10;
            var commits = GetCommits(subjectId);
            return commits.OrderByDescending(c => c.Commit.TimeStamp).Take(count).ToList();

        }

        public IEnumerable<CommitChange> GetCommitChangesWithoutMark()
        {
            return _context.Changes.Include(c => c.StudentSubject).ThenInclude(ss => ss.Subject).ThenInclude(s => s.Hometasks)
                .Include(c => c.StudentSubject).ThenInclude(ss => ss.Student)
                .Include(c => c.StudentSubject).ThenInclude(ss => ss.CommitChanges)
                .Include(c => c.StudentSubject).ThenInclude(ss => ss.Marks)
                .Include(c => c.Commit)
                .ThenInclude(c => c.Marks)
                .Include(c => c.Commit)
                .ThenInclude(c => c.Changes)
                .Where(cs => !cs.Commit.Marks.Any(m => cs.Path.StartsWith(m.Hometask.RepositoryPath)));
        }

        public IEnumerable<HometaskCommit> GetLastCommits(int subjectId, int hometaskId)
        {
            int count = 10;
            return GetCommits(subjectId, hometaskId).OrderByDescending(c => c.Commit.TimeStamp).Take(count).ToList();

        }

        public IEnumerable<HometaskCommit> GetCommits(int subjectId,
            int hometaskId)
        {
            return GetCommits(subjectId).OrderByDescending(c => c.Commit.TimeStamp).Where(c => c.Hometask.Id == hometaskId);
        }

        public IEnumerable<HometaskCommit> GetCommits()
        {
            return GetCommitChangesWithoutMark().Select(change => new HometaskCommit
            {
                Commit = change.Commit,
                Hometask = change.StudentSubject.Subject.Hometasks.Single(h => change.Path.StartsWith(h.RepositoryPath)),
                Student = change.StudentSubject.Student,
                Subject = change.StudentSubject.Subject
            });
        }
        //Last ten
        public IEnumerable<HometaskCommit> GetLastCommits()
        {
            int count = 10;
            return GetCommits().OrderByDescending(c => c.Commit.TimeStamp).Take(count).ToList();
        }

        public async Task AddMark(int commitId,
            string studentId,
            int hometaskId,
            double mark,
            string message)
        {
            if(mark < 0)
                throw new ArgumentException("Mark can't be less than zero");
            var hometask = _context.Hometasks.FirstOrDefault(h => h.Id == hometaskId);
            if(mark > hometask.MaxMark)
                throw new ArgumentException("Mark can't be more than max mark");
            var commit = _context.Commits.FirstOrDefault(c => c.Id == commitId);
            var foundMark = _context.Marks.FirstOrDefault(m => m.CommitId == commitId && m.HometaskId == hometaskId &&
                                                               m.StudentId == studentId);
            if (foundMark != null)
                throw new ArgumentException("Mark is already put");
            _context.Marks.Add(new Mark
            {
                CommitId = commitId,
                HometaskId = hometaskId,
                StudentId = studentId,
                SubjectId = hometask.SubjectId,
                Value = commit.TimeStamp <= hometask.Deadline ? mark : mark/2,
                Message = message
            });
            await _context.SaveChangesAsync();
        }
    }
}
