﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HometaskTracker.Domain.Concrete
{
    public class EfDbSubjectSubscriber : ISubjectSubscriber
    {
        private EfDbContext _context; 
        private DbSet<StudentSubject> _studentSubjects => _context.StudentSubject;

        public EfDbSubjectSubscriber(EfDbContext context)
        {
            _context = context;
        }
        public async Task SubscribeToSubject(int subjectId,
            string userId)
        {
            StudentSubject studentSubject = await GetSubscription(subjectId, userId);
            if (studentSubject != null)
                throw new ArgumentException("There is already studentSubject");

            var exists = await StudentAndSubjectExists(subjectId, userId);
            if (exists == false)
                throw new ArgumentException("Student or subject does not exist");

            _studentSubjects.Add(new StudentSubject
            {
                StudentId = userId,
                SubjectId = subjectId,
                SubscriptionStatus = SubscriptionStatus.Pending
            });
            await _context.SaveChangesAsync();
        }

        private async Task<StudentSubject> GetSubscription(int subjectId,
            string userId)
        {
            return await _studentSubjects.Where(ss => ss.StudentId == userId && ss.SubjectId == subjectId).FirstOrDefaultAsync();
        }

        private async Task<bool> StudentAndSubjectExists(int subjectId,
            string userId)
        {
            var student = await _context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();
            if (student == null)
                return false;

            var subject = await _context.Subjects.Where(s => s.SubjectId == subjectId).FirstOrDefaultAsync();
            if (subject == null)
                return false;

            return true;
        }

        public async Task ConfirmSubscription(int subjectId,
            string userId)
        {
            var subscription = await GetSubscription(subjectId, userId);
            if (subscription == null)
                throw new ArgumentException();

            subscription.SubscriptionStatus = SubscriptionStatus.Accepted;
            await _context.SaveChangesAsync();

        }

        public async Task UnsubscribeFromSubject(int subjectId,
            string userId)
        {
            StudentSubject studentSubject = await GetSubscription(subjectId, userId);
            if (studentSubject == null)
                throw new ArgumentException("Cannot find studentSubject");

            if(studentSubject.IsAccepted())
                throw new ArgumentException("It is already accepted");

            _studentSubjects.Remove(studentSubject);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<StudentSubject>> GetPending(int subjectId)
        {
            Subject subject = await _context.Subjects.Include(s => s.StudentSubjects).FirstOrDefaultAsync(s => s.SubjectId == subjectId);
            var pending =
                subject.StudentSubjects.Where(ss => ss.SubscriptionStatus == SubscriptionStatus.Pending);
            return _context.StudentSubject.Where(ss => pending.Any(ssp => ssp.StudentId == ss.StudentId && ssp.SubjectId == ss.SubjectId))
                .Include(ss => ss.Student).Include(ss => ss.Student);
        }

        public async Task AcceptStudent(int subjectId,
            string studentId)
        {
            var sub = await GetSubscription(subjectId, studentId);

            if(sub.SubscriptionStatus != SubscriptionStatus.Pending)
                throw new ArgumentException("Incorrect status for accepting");

            sub.SubscriptionStatus = SubscriptionStatus.NeedWebhook;
            await _context.SaveChangesAsync();
        }
    }
}