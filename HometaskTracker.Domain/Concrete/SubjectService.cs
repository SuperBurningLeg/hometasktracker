using HometaskTracker.Domain.Entities;
using HometaskTracker.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace HometaskTracker.Domain.Concrete
{
    public class SubjectService : ISubjectService
    {
        public User Teacher { get; set; }
        private readonly EfDbContext _context;
        public SubjectService(EfDbContext context)
        {
            this._context = context;
        }

        public IQueryable<Subject> Subjects {
            get
            {
                IQueryable<Subject> subjects = _context.Subjects
                    .Include(s => s.Hometasks)
                    .ThenInclude(h => h.Marks)
                    .Include(s => s.StudentSubjects)
                    .ThenInclude(ss => ss.Student)
                    .AsQueryable<Subject>();
                if (Teacher != null)
                    subjects = subjects.Where(s => s.TeacherId == Teacher.Id);
                return subjects;
            }
        }

        // TODO Check this shit

        private DbSet<Hometask> Hometasks => _context.Hometasks;

        public int CreateSubject(Subject subject)
        {
            subject.TeacherId = Teacher.Id;
            _context.Add(subject);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Debug.WriteLine(e);
                throw new ArgumentException();
            }
            return subject.SubjectId;
        }

        public void ChangeSubject(Subject subject)
        {
            subject.TeacherId = Teacher.Id;
            Subject foundSubject = FindSubjectById(subject);
            if (foundSubject == null)
                throw new EntityNotFoundException();
            foundSubject.SubjectName = subject.SubjectName;
            _context.Subjects.Update(foundSubject);
            _context.SaveChanges();
        }

        private Subject FindSubjectById(Subject subject)
        {

            return Subjects.FirstOrDefault(s => s.SubjectId == subject.SubjectId);
        }

        public void DeleteSubject(Subject subject)
        {
            subject.TeacherId = Teacher.Id;
            try
            {
                this._context.Subjects.Remove(Subjects.First(s => subject.SubjectId == s.SubjectId && subject.TeacherId == s.TeacherId));
            }
            catch (InvalidOperationException e)
            {
                throw new ArgumentException();
            }
            this._context.SaveChanges();
        }

        public async Task<Subject> GetSubjectByIdAsync(int subjectId)
        {
            return await this.Subjects.Where(s => s.SubjectId == subjectId).FirstOrDefaultAsync();
        }

        public int AddHometask(Subject subject,
            Hometask hometask)
        {
            Subject subjectFound = FindSubjectById(subject);

            if(subjectFound == null)
                throw new EntityNotFoundException();
            if(hometask.MaxMark <=0)
                throw new ArgumentException("Max mark cannot be less than or equal to zero");
            try
            {
                hometask.Subject = subject;
                Hometasks.Add(hometask);
                _context.SaveChanges();
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return -1;
            }

            return hometask.Id;
        }

        public bool ChangeHometask(Hometask hometask)
        {
            Subject subjectFound = GetSubjectByIdAsync(hometask.SubjectId).GetAwaiter().GetResult();

            if (subjectFound == null)
                throw new EntityNotFoundException();

            if (hometask.MaxMark <= 0)
                throw new ArgumentException("Max mark cannot be less than or equal to zero");

            try
            {

                Hometasks.First(h => h.Id == hometask.Id).CopyFrom(hometask);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public bool DeleteHometask(Hometask hometask)
        {
            Subject subjectFound = GetSubjectByIdAsync(hometask.SubjectId).GetAwaiter().GetResult();

            if (subjectFound == null)
                throw new EntityNotFoundException();

            try
            {
                var dbHometask = Hometasks.First(h => h.Id == hometask.Id);
                Hometasks.Remove(dbHometask);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}