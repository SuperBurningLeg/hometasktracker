﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HometaskTracker.Domain.Concrete
{
    public class SubscribedSubjectsRepository
    {
        public User User
        {
            get { return _user; }
            set { _user = value; }
        }

        private readonly EfDbContext _context;
        private User _user;

        public SubscribedSubjectsRepository(EfDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Subject> GetSubjects()
        {
            return User != null ? _user.StudentSubjects.Select(ss => ss.Subject) : _context.Subjects;
        }

        public IEnumerable<Subject> GetSubscribedSubjects()
        {
            return _user.StudentSubjects.Where(ss => ss.IsAccepted()).Select(ss => ss.Subject);
        }

        public IEnumerable<Hometask> GetHometasks(int subjectId)
        {
            var subject = GetSubjects().FirstOrDefault(s => s.SubjectId == subjectId);

            if(subject == null)
                throw new ArgumentException("subjectId is incorrect or user is not subscribed");

            return subject.Hometasks;
        }


        public IEnumerable<User> GetStudentsSubscribedTo(int subjectId)
        {
            var subject = GetSubjects().FirstOrDefault(s => s.SubjectId == subjectId);

            if (subject == null)
                throw new ArgumentException("subjectId is incorrect or user is not subscribed");

            return _context.StudentSubject.Include(ss => ss.Student).Where(ss => ss.IsAccepted() && ss.SubjectId == subjectId).Select(ss => ss.Student);
        }

        public SubscriptionStatus GetSubscriptionStatus(int subjectId)
        {
            StudentSubject studentSubject = GetStudentSubject(subjectId);
            if (studentSubject == null)
                throw new ArgumentException("Student is not subscribed to this subject or subject does not exists");
            return studentSubject.SubscriptionStatus;
        }

        private StudentSubject GetStudentSubject(int subjectId)
        {
            return _user.StudentSubjects.FirstOrDefault(ss => ss.SubjectId == subjectId);
        }

        private StudentSubject GetStudentSubjectFromDbContext(int subjectId)
        {
            return _context.StudentSubject.FirstOrDefault(ss => ss.SubjectId == subjectId && ss.StudentId == _user.Id);
        }

        public async Task SetRepository(int subjectId, string repositoryName)
        {
            var status = GetSubscriptionStatus(subjectId);

            if(status != SubscriptionStatus.NeedWebhook)
                throw new ArgumentException("Invalid status for adding a webhook.");

            var studentSubject = GetStudentSubjectFromDbContext(subjectId);
            studentSubject.RepositoryName = repositoryName;
            studentSubject.SubscriptionStatus = SubscriptionStatus.Accepted;
            int a = await _context.SaveChangesAsync();
        }
    }
}
