﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using SendGrid;

namespace HometaskTracker.Domain.Concrete
{
    public class TeacherInviter
    {
        private readonly EfDbContext _context;
        private readonly UserManager<User> _manager;
        private readonly SendGridOptions _options;
        private readonly string _tokenName = "EmailTeacherInvite";
        private readonly MailAddress _fromEmail = new MailAddress("admin@hometasktracker.herokuapp.com", "Hometasktracker");
        private readonly string _host =
#if DEBUG
            "http://localhost:44358";
#endif
#if RELEASE
        "http://hometasktracker.herokuapp.com";
#endif

        public TeacherInviter(EfDbContext context, UserManager<User> manager, IOptions<SendGridOptions> options)
        {
            _context = context;
            _manager = manager;
            _options = options.Value;
        }

        public async Task InviteTeacher(MailAddress email)
        {
            var user = await _manager.FindByEmailAsync(email.ToString());
            if (user != null)
            {
                if (user.IsTeacher.GetValueOrDefault())
                    throw new ArgumentException("This user is registered or  was already invited");
            }
            else
            {
                user = new User { UserName = email.ToString() };
                await _manager.CreateAsync(user);
                await _manager.SetEmailAsync(user, email.ToString());
                user.IsTeacher = true;
                await _manager.UpdateAsync(user);
            }
            string token = await _manager.GenerateUserTokenAsync(user, TokenOptions.DefaultProvider, _tokenName);
            var url = $"{_host}/TeacherAccount/RegisterByInvite?email={email}&token={UrlEncoder.Default.Encode(token)}";
            var text =
                $"Hi! You were  invited to be a teacher. </br> <a class=\"btn btn-default\" href=\"{url}\">Register</a> via github!.";
            var msg = new SendGridMessage();
            msg.AddTo(email.ToString());
            msg.From = _fromEmail;
            msg.Subject = "HometaskTracker Invite Teacher";
            msg.Html = text;

            var transportWeb = new SendGrid.Web(_options.SendgridApiKey);
            await transportWeb.DeliverAsync(msg);
        }

        public async Task<bool> CheckToken(string token, string email)
        {
            var user = await _manager.FindByEmailAsync(email);

            if(user == null)
                throw new ArgumentException("User with this email was not invited");

            return await _manager.VerifyUserTokenAsync(user, TokenOptions.DefaultProvider, _tokenName, token);
        }

        public async Task AddTeacher(string email, string userName)
        {
            var user = await _manager.FindByEmailAsync(email);
            if(user.IsTeacher.GetValueOrDefault())
                await _manager.SetUserNameAsync(user, userName);
            user.IsTeacher = true;
            await _manager.UpdateAsync(user);
        }
    }
}
