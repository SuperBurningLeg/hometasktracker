﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace HometaskTracker.Domain.Concrete
{
    public class WebhookAdder
    {

        private const string serverUrl =
#if DEBUG
            "http://home.vituha.net:5000/payload";
#endif
#if RELEASE
        "http://hometasktracker.herokuapp.com/payload";
#endif
        private const string githubUrl = "https://api.github.com";
        private readonly User _user;
        private readonly SubscribedSubjectsRepository _repository;

        public WebhookAdder(User user,
            SubscribedSubjectsRepository repository)
        {
            _user = user;
            _repository = repository;
        }

        // IT FUCKING WORKS!
        public async Task<bool> AddWebhook(int subjectId,
            string repositoryName, string token)
        {
            var subject = _repository.GetSubjects().Where(s => s.SubjectId == subjectId);

            if (subject == null)
                throw new ArgumentException("There is no such a subject");

            var status = _repository.GetSubscriptionStatus(subjectId);

            if (status == SubscriptionStatus.Accepted || status == SubscriptionStatus.Pending)
                throw new ArgumentException("Incorrect student subscription status");

            var userName = _user.UserName;
            string webhookAddress = GetWebhookAddress(repositoryName, token, userName);
            var parameters = CreateParams();
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                Uri reqUri = new Uri(githubUrl + webhookAddress);
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, reqUri) {Content = content};
                message.Headers.Add("user-agent", "hometasktracker");
                var response = await client.SendAsync(message);
                var respString = await response.Content.ReadAsStringAsync();
                return response.IsSuccessStatusCode || response.StatusCode == (HttpStatusCode)(422);
            }
        }

        private static object CreateParams()
        {
            var parameters = new
            {
                name = "web",
                active = true,
                events = new List<string>
                {
                    "push"
                },
                config = new
                {
                    url = serverUrl,
                    content_type = "json"
                }
            };
            return parameters;
        }

        private static string GetWebhookAddress(string repositoryName, string token, string userName)
        {
            return "/repos/" + userName + "/" + repositoryName + "/hooks?access_token=" + token;
        }
    }
}
