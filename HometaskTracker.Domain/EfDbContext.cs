using System.Runtime.InteropServices.ComTypes;
using Microsoft.EntityFrameworkCore;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HometaskTracker.Domain
{
    public class EfDbContext : IdentityDbContext<User>
    {
        //public DbSet<User> AppUsers {get;set;}

        public virtual DbSet<Subject> Subjects{get;set;}
        public virtual DbSet<Mark> Marks { get; set; }
        public virtual DbSet<Hometask> Hometasks { get; set; }
        public virtual DbSet<StudentSubject> StudentSubject { get; set; }
        public virtual DbSet<Commit> Commits { get; set; }
        public virtual DbSet<CommitChange> Changes { get; set; }

        public EfDbContext() : base(new DbContextOptions<EfDbContext>())
        {
            
        }

        public EfDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var commits = builder.Entity<Commit>().ToTable("Commits");
            commits.HasKey(c => c.Id);
            commits.HasMany(c => c.Marks).WithOne(m => m.Commit).HasForeignKey(m => m.CommitId);


            var changes = builder.Entity<CommitChange>().ToTable("Changes");
            changes.HasOne(c => c.Commit).WithMany(com => com.Changes).HasForeignKey(c => c.CommitId);
            changes.HasOne(c => c.StudentSubject)
                .WithMany(ss => ss.CommitChanges)
                .HasForeignKey(c => new {c.StudentId, c.SubjectId});


            builder.Entity<Hometask>().HasKey(h => h.Id);
            builder.Entity<Hometask>().HasOne(h => h.Subject)
                .WithMany(s => s.Hometasks)
                .HasForeignKey(s => s.SubjectId);
            
            builder.Entity<Mark>().HasKey(m => m.Id);
            builder.Entity<Mark>().HasOne(m => m.StudentSubject)
                .WithMany(ss => ss.Marks)
                .HasForeignKey(m => new {m.StudentId, m.SubjectId});
            builder.Entity<Mark>().HasOne(m => m.Hometask)
                .WithMany(h => h.Marks)
                .HasForeignKey(m => m.HometaskId);
            builder.Entity<Mark>().HasIndex(m => new {m.CommitId, m.HometaskId, m.StudentId}).IsUnique();
            
            var user = builder.Entity<User>()
            .ToTable("AspNetUsers");
            user.HasMany(u => u.Roles).WithOne().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithOne().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithOne().HasForeignKey(ul => ul.UserId);
            user.Property(u => u.UserName).IsRequired();

            // One to many - subjects to students

            builder.Entity<Subject>().Property(s => s.SubjectName).IsRequired();
            builder.Entity<Subject>().ToTable("Subjects").HasKey(x => x.SubjectId);
            builder.Entity<Subject>().HasOne(s => s.Teacher).WithMany(u => u.Subjects).HasForeignKey(s => s.TeacherId);

            builder.Entity<StudentSubject>().HasKey(x => new {x.StudentId, x.SubjectId});
            builder.Entity<StudentSubject>().HasOne(s => s.Student)
                .WithMany(s => s.StudentSubjects)
                .HasForeignKey(x => x.StudentId);
            builder.Entity<StudentSubject>().HasOne(x => x.Subject)
                .WithMany(x => x.StudentSubjects)
                .HasForeignKey(x => x.SubjectId);

            
        }
    }
}