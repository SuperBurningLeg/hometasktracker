﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Domain.Entities
{
    public class Commit
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Url { get; set; }
        public ICollection<CommitChange> Changes { get; set; }
        public ICollection<Mark> Marks { get; set; }
    }
}
