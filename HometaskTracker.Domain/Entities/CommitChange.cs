﻿namespace HometaskTracker.Domain.Entities
{
    public class CommitChange
    {
        public int Id { get; set; }
        public int CommitId { get; set; }
        public string Path { get; set; }
        public string StudentId { get; set; }
        public int? SubjectId { get; set; }
        public StudentSubject StudentSubject { get; set; }
        public Commit Commit { get; set; }
    }
}