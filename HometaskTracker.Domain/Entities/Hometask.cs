﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HometaskTracker.Domain.Entities
{
    public class Hometask
    {
        private DateTime _deadline;
        public int Id { get; set; }
        public string Name { get; set; }
        public string RepositoryPath { get; set; }
        public string Description { get; set; }

        public DateTime Deadline
        {
            get { return _deadline; }
            set { _deadline = new DateTime(value.Year, value.Month, value.Day, 23, 59, 59); }
        }

        public int SubjectId { get; set; }
        public Subject Subject { get; set; }

        public ICollection<Mark> Marks { get; set; }
        public int MaxMark { get; set; }

        public void CopyFrom(Hometask hometask)
        {
            
            this.Name = hometask.Name;
            this.RepositoryPath = hometask.RepositoryPath;
            this.Description = hometask.Description;
            this.Deadline = hometask.Deadline;
            this.MaxMark = hometask.MaxMark;
        }
    }
}
