﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Domain.Entities
{
    public class HometaskCommit
    {
        public Commit Commit { get; set; }
        public Hometask Hometask { get; set; }
        public User Student { get; set; }
        public Subject Subject { get; set; }
    }

    public class HometaskCommitEqualityComparer : IEqualityComparer<HometaskCommit>
    {
        public bool Equals(HometaskCommit x,
            HometaskCommit y)
        {
            return x.Hometask.Id == y.Hometask.Id &&
                   x.Commit.Id == y.Commit.Id &&
                   x.Student.Id == y.Student.Id;
        }

        public int GetHashCode(HometaskCommit obj)
        {
            return obj.Commit.Id.GetHashCode() + obj.Hometask.GetHashCode() + obj.Student.Id.GetHashCode();
        }
    }
}
