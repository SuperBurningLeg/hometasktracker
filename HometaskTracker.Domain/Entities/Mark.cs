﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Domain.Entities
{
    public class Mark
    {
        public int Id { get; set; }

        public double Value { get; set; }
        public string Message { get; set; }

        public string StudentId { get; set; }
        public int SubjectId { get; set; }
        public StudentSubject StudentSubject { get; set; }

        public int HometaskId { get; set; }
        public Hometask Hometask { get; set; }

        public int CommitId { get; set; }
        public Commit Commit { get; set; }
    }
}
