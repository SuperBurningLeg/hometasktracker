﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Domain.Entities
{
    public enum SubscriptionStatus
    {
        Pending, NeedWebhook, Accepted
    }
    public class StudentSubject
    {
        public string StudentId { get; set; }
        public User Student { get; set; }

        public int? SubjectId { get; set; }
        public Subject Subject { get; set; }

        public string RepositoryName { get; set; }

        public SubscriptionStatus SubscriptionStatus { get; set; }

        public ICollection<Mark> Marks { get; set; }
        public ICollection<CommitChange> CommitChanges { get; set; }

        public bool IsAccepted()
        {
            return SubscriptionStatus == SubscriptionStatus.Accepted;
        }
    }
}
