using System.Collections.Generic;

namespace HometaskTracker.Domain.Entities
{
    public class Subject
    {
        public Subject()
        {
            
        }

        public Subject(string name)
        {
            this.SubjectName = name;
        }

        public Subject(int id,
            string name)
        {
            this.SubjectId = id;
            this.SubjectName = name;
        }

        public int SubjectId {get;set;}
        public string SubjectName {get;set;}
        public int CourseNumber {get;set;}
        public string TeacherId { get; set; }
        public User Teacher { get; set; }

        public ICollection<StudentSubject> StudentSubjects{ get; set; }
        public ICollection<Hometask> Hometasks { get; set; }

    }
}