using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore; 


namespace HometaskTracker.Domain.Entities
{
    public class User : IdentityUser
    {
        //public string UserName {get;set;}
        //public string PasswordHash {get;set;}
        public bool? IsTeacher {get;set;}
        public string Name {get;set;}
        public string Surname {get;set;}
        public int? CourseNumber {get;set;}

        public override string NormalizedUserName
        {
            get
            {
                return UserName.ToUpper();
            }
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                return;
            }
        }

        public string AvatarUrl { get; set; }
        public string ApiSecret { get; set; }

        [NotMapped]
        public bool PersonalDataFilled
        {
            get
            {
                var hasName = !string.IsNullOrEmpty(Name);
                return hasName;
            }
        }

        public ICollection<StudentSubject> StudentSubjects { get; set; }
        public ICollection<Subject> Subjects { get; set; }

    }
}