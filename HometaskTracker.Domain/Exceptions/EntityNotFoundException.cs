using System;
using System.Runtime.Serialization;

namespace HometaskTracker.Domain.Exceptions
{
    public class EntityNotFoundException : ArgumentException
    {
        public EntityNotFoundException() : base("Entity was not found. Check the ID.")
        {
        }
    }
}