"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by kostya on 12/8/2016.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var SecretCodeComponent = (function () {
    function SecretCodeComponent(http) {
        this.http = http;
        this.code = null;
    }
    SecretCodeComponent.prototype.getCode = function () {
        var _this = this;
        this.http.get('/api/apiauthentication/newcode')
            .toPromise()
            .then(function (res) {
            _this.code = res.text();
        });
    };
    SecretCodeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'secretcode',
            template: "<button class=\"btn btn-default\" *ngIf=\"code==null\" (click)=\"getCode()\">Get New Secret Api Key</button>\n        <input type=\"text\" class=\"code form-control\" *ngIf=\"code!=null\" readonly value=\"{{code}}\" />\n        ",
            styles: [".code { width: 200px; display: inline-block;}"]
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SecretCodeComponent);
    return SecretCodeComponent;
}());
exports.SecretCodeComponent = SecretCodeComponent;
//# sourceMappingURL=secretcode.component.js.map