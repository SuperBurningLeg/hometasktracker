/**
 * Created by kostya on 12/8/2016.
 */
import {Component} from "@angular/core";
import {Http} from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'secretcode',
    template:
        `<button class="btn btn-default" *ngIf="code==null" (click)="getCode()">Get New Secret Api Key</button>
        <input type="text" class="code form-control" *ngIf="code!=null" readonly value="{{code}}" />
        `,
    styles: [`.code { width: 200px; display: inline-block;}`]
})
export class SecretCodeComponent {
    private code: string;
    constructor(private http: Http) {
        this.code = null;
    }

    getCode() {
        this.http.get('/api/apiauthentication/newcode')
            .toPromise()
            .then((res) => {
                this.code = res.text();
            })
    }
}