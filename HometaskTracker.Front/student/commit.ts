/**
 * Created by kostya on 11/17/2016.
 */
export class Commit {
    id: number;
    timeStamp: Date;
    mark: Mark;
}

export class Mark {
    value: number;
    message: string;
}