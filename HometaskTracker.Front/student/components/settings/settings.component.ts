import {Component, OnInit} from '@angular/core';

import {SettingsServerService} from '../../services/settings-server.service';
import {SubscribedSubject} from '../../subscribed-subject';
import {Subject} from '../../subject';
import {SubjectSubscriptionSharedService} from "../../services/subject-subscription-shared.service";

@Component({
    moduleId: module.id,
    selector: 'settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css']
})
export class SettingsComponent implements OnInit {
    subscribedSubjects : SubscribedSubject[];
    subjects : Subject[];
    chosenSubject: Subject;
    errorMessage: string;
    constructor(private settingsService: SettingsServerService, private subjectSubscriptionService : SubjectSubscriptionSharedService) {
        this.subscribedSubjects = [];
        this.errorMessage = null;
    }

    public ngOnInit() {
        this.refreshData();

    }

    getSubscribedSubjects(): Promise<void> {
        return this.settingsService.subscribedSubjects().then(subjects => {this.subscribedSubjects = subjects});
    }

    getSubjects() : Promise<void> {
        return this.settingsService.subjects().then(subjects => {this.subjects = subjects});
    }

    subscribe() {
        this.settingsService.subscribeToSubject(this.chosenSubject)
            .then(() => {
            this.refreshData();

                });
    }

    refreshData() : Promise<void> {
        return this.getSubscribedSubjects()
            .then(() => this.getSubjects());
    }

    unsubscribe(subject: SubscribedSubject) {
        this.settingsService.unSubscribeToSubject(subject)
            .then(() => this.refreshData());
    }

    addWebhook(subject : SubscribedSubject) {
        this.settingsService.addWebhook(subject).then((success) => {
            if(!success)
                this.errorMessage = "Could not add webhook";
            else
            {
                this.subjectSubscriptionService.onSubscribedSubjectsChange.emit(0);
                this.refreshData();
            }
        })
    }
} 