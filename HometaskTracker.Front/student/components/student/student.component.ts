import {Component, OnInit, OnChanges, SimpleChanges, ViewContainerRef} from '@angular/core';

import {SubjectServerService} from './../../services/subject-server.service';
import {Subject} from './../../subject';
import {SubjectSubscriptionSharedService} from "../../services/subject-subscription-shared.service";
import {HometaskCommit} from "../../../teacher/hometask-commit";

@Component({
    moduleId: module.id,
    selector: 'student',
    templateUrl: 'student.component.html',
    styleUrls: ['student.component.css']
})
export class StudentComponent implements OnInit, OnChanges {
    ngOnChanges(changes: SimpleChanges): void {
        this.getSubjects();
    }
    subjects : Subject[];
    private viewContainerRef:ViewContainerRef;
    constructor(private subjectService : SubjectServerService, private subjectSubscriptionService : SubjectSubscriptionSharedService, viewContainerRef:ViewContainerRef){
        this.subjectSubscriptionService.onSubscribedSubjectsChange.subscribe((data) => {this.getSubjects()});
        this.viewContainerRef = viewContainerRef;
    }

    ngOnInit() {
        this.getSubjects();
    }

    getSubjects() {
        this.subjectService.getSubscribedSubjects().then(subjects => {this.subjects = subjects});
    }
}