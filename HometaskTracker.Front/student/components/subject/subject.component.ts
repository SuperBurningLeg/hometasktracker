import {
    Component, OnInit, OnChanges, Input, SimpleChange, SimpleChanges, ViewChild,
    ChangeDetectionStrategy
} from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { LOCALE_ID } from '@angular/core';

import {SubjectServerService} from './../../services/subject-server.service';
import {Subject} from './../../subject';
import {Hometask} from "../../hometask";
import {ModalDirective} from "ng2-bootstrap";
import {Student} from "../../student";
import {Commit} from "../../commit";
import {forEach} from "@angular/router/src/utils/collection";
import {isUndefined} from "util";
import {isNullOrUndefined} from "util";

@Component({
    moduleId: module.id,
    selector: 'subject',
    templateUrl: 'subject.component.html',
    styleUrls: ['subject.component.css']
})
export class SubjectComponent implements OnInit {
    @ViewChild('childModal') public childModal:ModalDirective;
    @ViewChild('hometask') public hometask:ModalDirective;
    subjectId: number;
    subject: Subject;
    hometasks: Hometask[];
    students: Student[];
    commits: Commit[];
    curHometask: Hometask;
    marks: HometaskStudentMark[];
    clickedHometask : Hometask;
    constructor(private route: ActivatedRoute, private location: Location, private subjectService : SubjectServerService) {
        this.subject = null;
        this.commits = [];
        this.clickedHometask = new Hometask();
        this.clickedHometask.deadline = new Date(1);
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.subjectId = id;
            this.refreshData();
        });
    }

    getSum(student: Student) {
        let sum = this.marks.reduce((prev, cur, i, a) => {
            if(cur.studentId == student.id)
                return prev + cur.mark;
            else
                return prev;
        }, 0);
        sum = sum == Number.NEGATIVE_INFINITY ? 0 :
            sum == Number.POSITIVE_INFINITY ? 0 : sum;

        return  sum.toString();
    }

    refreshData() {
        this.getSubscribedSubjects()
            .then(() => this.getHometasks())
            .then(() => this.getStudents())
            .then(() => {
                this.marks = [];
                this.students.forEach(s => {
                    this.subjectService.getTopMarks(s, this.subject).then((m) => {
                        let i = 0;
                        m.forEach((mark) => {
                            this.marks.push({
                                mark : mark,
                                studentId : s.id,
                                hometaskId : this.hometasks[i].id
                            });
                            i += 1;
                        })
                    });
                })
            });
    }

    getSubscribedSubjects(): Promise<void> {
        return this.subjectService.getSubscribedSubjects().then(ss => {
            let subject = ss.find(s => s.id == this.subjectId);
            if(subject != null)
                this.subject = subject;
        });
    }

    getHometasks(): Promise<void> {
        return this.subjectService.getHometasks(this.subject)
            .then(hs => {this.hometasks = hs; this.clickedHometask = this.hometasks[0]});
    }

    private getStudents(): Promise<void> {
        return this.subjectService.getStudents(this.subject).then(ns => {this.students = ns;});
    }
    public showCommits(student : Student, hometask: Hometask):void {
        this.curHometask = hometask;
        this.subjectService.getCommits(hometask, student)
            .then((commits) => {
            this.commits = commits.sort((c1, c2) => new Date(c1.timeStamp).getTime() - new Date(c2.timeStamp).getTime())}
            ).then(
            () => this.childModal.show()
        );

    }

    public getTopMark(student : Student, hometask: Hometask) : string{
        let mark = this.marks.find((m) => m.studentId == student.id && m.hometaskId == hometask.id);

        if(mark == null || mark == undefined)
            return '';
        if(mark.mark == null || mark.mark == undefined || mark.mark <=0)
            return '';

        return mark.mark.toString();

    }

    public hideCommits(childModal : ModalDirective):void {
        childModal.hide();
    }

    hasDeadlinePassed(date: Date, deadline: Date) {
        let passed = new Date(date).getTime() > new Date(deadline).getTime();
        return passed;
    }

    ensureDate(date) {
        date = new Date(date);
        return new Date(date.getTime() + date.getTimezoneOffset()* 60000)
    }

    clickHometask(hometask: Hometask) {
        this.clickedHometask = hometask;
        this.hometask.show();
    }

    closeHometask(hometask : ModalDirective) {
        hometask.hide();
    }


}

class HometaskStudentMark {
    hometaskId: number;
    studentId: string;
    mark: number
}