import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable, ObservableInput }     from 'rxjs/Observable';

import {SubscribedSubject} from './../subscribed-subject';
import {Subject} from './../subject';
import './../rxjs-operators';

@Injectable() 
export class SettingsServerService {
    readonly serverAddress: string = "/";
    readonly settingsUrl: string = "API/Student/Settings";
    readonly subscribeAction: string = "/SubscribeToSubject";
    readonly subscribedSubjectsUrl: string = "/SubscribedSubjects";
    readonly subjectsUrl: string = "/Subjects";
    readonly webhookUrl: string = "/Webhook";

    constructor(private http: Http) {
    }

    extractSubjects(res: Response): Subject[] {
        let subjects = res.json() || {};
        return subjects;
    }

    subscribeToSubject(subject: Subject): Promise<void> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(this.serverAddress + this.settingsUrl + this.subscribeAction, JSON.stringify(subject), options).toPromise().then(() => {
            return;
        });
    }

    subscribedSubjects(): Promise<SubscribedSubject[]> {
        return this.http.get(this.serverAddress + this.settingsUrl + this.subscribedSubjectsUrl).toPromise().then(this.extractSubscribedSubjects);
    }

    extractSubscribedSubjects(res: Response): SubscribedSubject[] {
        let subjects = res.json() || {};
        return subjects;
    }

    subjects(): Promise<Subject[]> {
        return this.http.get(this.serverAddress + this.settingsUrl + this.subjectsUrl).toPromise().then(this.extractSubjects);
    }

    unSubscribeToSubject(subject: SubscribedSubject): Promise<void> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers, body: JSON.stringify(subject)});
        return this.http.delete(this.serverAddress + this.settingsUrl + this.subscribeAction, options).toPromise().then(() => {
            return;
        });
    }

    addWebhook(subject: SubscribedSubject): Promise<boolean> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(this.serverAddress + this.settingsUrl + this.webhookUrl, JSON.stringify(subject), options).toPromise().then(this.handleAddWebhookResponse);
    }

    handleAddWebhookResponse(res: Response): boolean {
        let result = res.json();
        let code = result.code;
        if(code == 200)
            return true;
        else
            return false;
    }
}