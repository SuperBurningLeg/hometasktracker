import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';

import {Subject} from './../subject';
import './../rxjs-operators';
import {Hometask} from "../hometask";
import {Student} from "../student";
import {Commit} from "../commit";

@Injectable() 
export class SubjectServerService {
    readonly serverAddress: string = "/";
    readonly studentsUrl : string = "API/Student";
    readonly commitsUrl : string = "API/Student/Commits";
    readonly subjectsAction = "/Subjects";
    readonly hometasksActionWithParam : string = "/Hometasks?subjectId=";
    readonly studentsActionWithParam : string = "/Students?subjectId="
    readonly subscribeAction : string
    constructor(private http : Http) {}

    extractSubjects(res: Response) : Subject[]{
        let subjects = res.json() || {};
        return subjects ;
    }

    getSubscribedSubjects() : Promise<Subject[]> {
        return this.http.get(this.serverAddress + this.studentsUrl + this.subjectsAction).toPromise()
        .then(this.extractSubjects);
    }

    getHometasks(subject:Subject) : Promise<Hometask[]> {
        return this.http.get(this.serverAddress + this.studentsUrl + this.hometasksActionWithParam + subject.id.toString()).toPromise()
            .then(this.extractHometasks);
    }

    getStudents(subject: Subject): Promise<Student[]> {
        return this.http.get(this.serverAddress + this.studentsUrl + this.studentsActionWithParam + subject.id.toString()).toPromise()
            .then(this.extractStudents);
    }

    private extractStudents(res: Response) : Student[] {
        let students = res.json() || {};
        return students;
    }

    private extractHometasks(res: Response) : Hometask[] {
        let hometasks = res.json() || {};
        return hometasks ;
    }

    getCommits(hometask: Hometask, student: Student) : Promise<Commit[]>{
        return this.http.get(this.serverAddress + this.commitsUrl + "/?studentId=" + student.id + "&hometaskId=" + hometask.id.toString()).toPromise()
            .then(this.extractCommits)
    }

    getTopMarks(student: Student, subject: Subject) : Promise<number[]>{
        return this.http.get(this.serverAddress + this.commitsUrl + "/GetTopMarks?studentId=" + student.id + "&subjectId=" + subject.id).toPromise()
            .then((res: Response) => res.json());
    }

    private extractCommits(res: Response) : Commit[] {
        let commits = res.json() || {};
        return commits;
    }
}