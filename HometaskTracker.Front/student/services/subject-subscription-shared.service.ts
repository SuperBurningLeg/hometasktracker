import {Injectable, EventEmitter} from "@angular/core";
/**
 * Created by kostya on 11/17/2016.
 */
@Injectable()
export class SubjectSubscriptionSharedService {
    onSubscribedSubjectsChange: EventEmitter<number> = new EventEmitter<number>();
}