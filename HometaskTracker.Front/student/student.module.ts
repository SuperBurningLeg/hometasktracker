import {NgModule, LOCALE_ID} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';

import {StudentComponent} from './components/student/student.component';
import {SettingsComponent} from './components/settings/settings.component';
import {SubjectComponent} from './components/subject/subject.component';
import {SubjectServerService} from './services/subject-server.service';
import {SettingsServerService} from './services/settings-server.service';
import {SubjectSubscriptionSharedService} from "./services/subject-subscription-shared.service";
import {ModalModule} from "ng2-bootstrap";
import {SecretCodeComponent} from "../shared/components/secretcode.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [BrowserModule, RouterModule.forRoot([
                 {path: '', component : SettingsComponent},
      { path: 'settings', component: SettingsComponent },
      { path: ':id', component: SubjectComponent }
    ]), HttpModule, FormsModule, ModalModule, CommonModule],
    declarations: [StudentComponent, SettingsComponent, SubjectComponent, SecretCodeComponent],
    providers: [{ provide: LOCALE_ID, useValue: "en-GB" }, SubjectServerService, SettingsServerService, SubjectSubscriptionSharedService],
    bootstrap: [StudentComponent]
})

export class StudentModule { }