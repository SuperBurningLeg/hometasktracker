/**
 * Created by kostya on 11/17/2016.
 */
export class Student {
    id: string;
    name: string;
    avatarUrl: string;
}