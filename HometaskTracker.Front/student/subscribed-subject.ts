import {SubscriptionStatus} from './subscription-status';

export class SubscribedSubject {
    studentId: string;
    studentName: string;
    subjectId: number;
    repositoryName : string;
    subscriptionStatus : number;
    subjectName : string;
}