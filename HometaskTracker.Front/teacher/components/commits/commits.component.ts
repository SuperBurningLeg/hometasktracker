import {Component, OnInit, ViewChild} from '@angular/core';
import {Router, Params, ActivatedRoute} from '@angular/router';
import {HometaskCommit} from "../../hometask-commit";
import {TeacherSubjectServerService} from "../../services/subject-server.service";
import {CommitsServerService} from "../../services/commit-server-service";
import {Subject} from "../../subject";
import {Hometask} from "../../hometask";
import {HometaskServerService} from "../../services/hometask-server.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/interval';
import {ModalDirective} from "ng2-bootstrap";

@Component({
    moduleId: module.id,
    selector: 'commits',
    templateUrl: 'commits.component.html',
    styleUrls: ['commits.component.css']
})

export class CommitsComponent implements OnInit {
    constructor(private router: Router, private commitsService : CommitsServerService,
                private subjectService: TeacherSubjectServerService, private route: ActivatedRoute,
    private hometasksService: HometaskServerService) {
        this.errorMessage = null;
        this.subjectId = null;
        this.selectedSubject = null;
        this.commitsType = "Subject"
        this.commits = []
        this.subjects = []
        this.hometasks = []
        this.selectedHometaskCommit = new HometaskCommit()
    }
    @ViewChild('popup') public popup:ModalDirective;
    commits: HometaskCommit[];
    subjectId : number;
    errorMessage : string;
    subjects: Subject[];
    hometasks: Hometask[];
    selectedSubject: Subject;
    selectedHometask: Hometask;
    commitsType: string;
    selectedHometaskCommit : HometaskCommit;

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            let id = params['id'];
            if(id != null && id != undefined)
            {
                this.commitsType = "Hometask";
                this.subjectId = id;
            }
            this.refresh();
            Observable.interval(5000).subscribe(() => this.refresh());
        });
    }

    getSubjectCommits() {
        this.subjectService.getSubjects().toPromise()
            .then((s : Subject[]) => {
            if(this.subjects.find(subj => s.find(subjNew => subj.id == subjNew.id) == null)
            || s.find(subj => this.subjects.find(subjNew => subj.id == subjNew.id) == null))
                this.subjects = s;
            let prevSubjectInNewData = this.selectedSubject == null ? null : this.subjects.find(subj => subj.id == this.selectedSubject.id)
                if(prevSubjectInNewData == null)
                    this.selectedSubject = this.subjects[0];
                else
                    this.selectedSubject = prevSubjectInNewData
            })
            .then(() => {
                if(this.selectedSubject == null)
                    return [];
            this.selectSubjectChanged();
        })

    }

    refresh() {
        if(this.subjectId == null)
            this.getSubjectCommits();
        else
            this.getHometaskCommits();
    }

    getHometaskCommits() {
        this.hometasksService.getHometasks(this.subjectId).then((h) => {
            if(
            this.hometasks.find(home => h.find(homeNew => home.id == homeNew.id) == null)
            || h.find(home => this.hometasks.find(homeNew => home.id == homeNew.id) == null))
            this.hometasks = h

        })
            .then(() =>
            {
                let prevHometaskInNewData = this.selectedHometask == null ? null : this.hometasks.find(home => home.id == this.selectedHometask.id)
                if(prevHometaskInNewData == null)
                    this.selectedHometask= this.hometasks[0];
                else
                    this.selectedHometask = prevHometaskInNewData
            })
            .then(() => {
                if(this.selectedHometask == null)
                    return;
            this.selectHometaskChanged()});
    }

    ensureDate(date) {
        date = new Date(date);
        return new Date(date.getTime() + date.getTimezoneOffset()* 60000)
    }

    addMark(commit: HometaskCommit) {
        this.commitsService.addMark(commit).then(() => this.refresh()).catch(e => this.errorMessage = e);
        return 0;
    }

    selectSubjectChanged() {
        this.commitsService.getLastCommits(this.selectedSubject).catch(e => this.errorMessage = e).then((commits) => this.commits = commits);
    }
    selectHometaskChanged() {
        this.commitsService.getLastCommitsByHometask(this.subjectId, this.selectedHometask).catch(e => this.errorMessage = e).then((commits) => this.commits = commits);
    }

    popModal(commit: HometaskCommit) {
        this.selectedHometaskCommit = commit;
        this.popup.show();
    }

    unpopModal(submit: boolean) {
        this.popup.hide();
        if(submit)
            this.addMark(this.selectedHometaskCommit);
        this.selectedHometaskCommit = new HometaskCommit();
    }
}/**
 * Created by kostya on 12/5/2016.
 */
