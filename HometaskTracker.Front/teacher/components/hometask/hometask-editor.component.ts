/**
 * Created by kostya on 12/11/2016.
 */
import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Hometask} from "../../hometask";
import {JSONP_HOME} from "@angular/http/src/backends/browser_jsonp";
@Component({
    moduleId: module.id,
    selector: 'hometask-editor',
    templateUrl: 'hometask-editor.component.html',
    styleUrls: ['hometask-editor.component.css']
})
export class HometaskEditorComponent {
    minDate : Date;
    @Input()
    buttonText: string;
    _hometaskToChange: Hometask;
    @Input()
    set hometaskToChange(h: Hometask) {
            this._hometaskToChange = Object.assign({}, h);
            this.minDate = this.minDate < this._hometaskToChange.deadline ? this.minDate : new Date(this._hometaskToChange.deadline);
    }
    get hometaskToChange() {
        return this._hometaskToChange;
    }
    @Output()
    onSubmited = new EventEmitter<Hometask>();
    constructor() {
        this.minDate = new Date(Date.now());
        this._hometaskToChange = new Hometask();
        this._hometaskToChange.deadline = this.minDate;
    }

    dateChanged(e: Event) {
        let input : HTMLInputElement = <HTMLInputElement> e.target;
        this._hometaskToChange.deadline = new Date(Date.parse(input.value));
    }

    submit() {
        this.onSubmited.emit(this._hometaskToChange);
    }
}