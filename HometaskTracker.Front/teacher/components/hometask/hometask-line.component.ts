/**
 * Created by kostya on 12/11/2016.
 */
import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Hometask} from "../../hometask";
import {HometaskServerService} from "../../services/hometask-server.service";
@Component({
    moduleId: module.id,
    selector: 'hometask-line',
    templateUrl: 'hometask-line.component.html',
    styleUrls: ['hometask-line.component.ts']
})
export class HometaskLineComponent {
    _hometask: Hometask;
    @Input()
    set hometask(h: Hometask) {
        this._hometask = h;
    }
    get hometask() {
        return this._hometask;
    }
    @Output()
    onError = new EventEmitter<string>();

    isMoreInfoOpened: boolean;
    @Output()
    onChanged = new EventEmitter();

    @Output()
    onDelete = new EventEmitter();

    constructor(private hometaskService: HometaskServerService) {
    }

    deleteHometask() {
        this.hometaskService.deleteHometask(this.hometask).catch(e => this.onError.emit(e)).then(() => this.onChanged.emit());
        this.onDelete.emit();
    }

    changeHometask(hometask: Hometask) {
        console.log(hometask)
        this.hometaskService.changeHometask(hometask).catch(e => this.onError.emit(e))
            .then(() => this.onChanged.emit());
        this.isMoreInfoOpened = false;
    }

    openMoreInfo() {
        this.isMoreInfoOpened = true;
    }

    closeMoreInfo() {
        this.isMoreInfoOpened = false;
    }
}
