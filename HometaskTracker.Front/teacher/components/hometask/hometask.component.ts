import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router}   from '@angular/router';
import { Location }                 from '@angular/common';

import {HometaskServerService} from './../../services/hometask-server.service';
import {Hometask} from './../../hometask';
import {TeacherSubjectServerService} from './../../services/subject-server.service';
import {Subject} from "../../subject";

@Component({
    moduleId: module.id,
    selector: 'hometasks',
    templateUrl: 'hometask.component.html',
    styleUrls: ['hometask.component.css']
})

export class HometaskComponent implements OnInit {
    subjectId : number;
    subjectName: string;
    hometasks : Hometask[];
    errorMessage: string;
    subject: Subject;
    addHometaskOpened : boolean;

    constructor(private subjectService : TeacherSubjectServerService,
    private hometaskService : HometaskServerService,
       private route: ActivatedRoute,
  private location: Location, private router : Router)
    {
        this.hometasks = [];
        this.subjectName = "";
        this.subject = new Subject(0, '');
    }
    

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.subjectId = id;
        });
        this.subjectService.getSubjectById(this.subjectId).catch(e => this.errorMessage = e)
            .then(subject => {
                this.subjectName = subject.name;
                this.subject = new Subject(this.subjectId, this.subjectName);
                this.getHometasks();
            },
                        error => this.errorMessage = error);
}

    getHometasks() : void {
        this.hometaskService.getHometasks(this.subjectId)
            .then(hometasks => this.hometasks = hometasks)
            .catch(error => this.errorMessage = error);
    }

    createHometask(hometask: Hometask)
    {
        hometask.subjectId = this.subjectId;
        this.hometaskService.createHometask(hometask).then(() => {this.getHometasks()})
                                                .catch((e) => this.errorMessage = e.message);
        this.untoggleToAdd();
    }

    toggleToAdd()
    {
        this.addHometaskOpened = true;
    }

    untoggleToAdd() {
        this.addHometaskOpened = false;
    }
    gotoSubjects() {
        this.router.navigateByUrl('/subjects')
    }
}