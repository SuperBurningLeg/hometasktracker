import {
    Component, OnInit, OnChanges, Input, SimpleChange, SimpleChanges, ViewChild,
    ChangeDetectionStrategy
} from '@angular/core';
import {ActivatedRoute, Params, Router}   from '@angular/router';
import { Location }                 from '@angular/common';
import { LOCALE_ID } from '@angular/core';

import {TeacherSubjectServerService} from './../../services/subject-server.service';
import {Subject} from './../../subject';
import {Hometask} from "../../hometask";
import {ModalDirective} from "ng2-bootstrap";
import {forEach} from "@angular/router/src/utils/collection";
import {isUndefined} from "util";
import {isNullOrUndefined} from "util";
import {Student} from "../../../student/student";
import {HometaskServerService} from "../../services/hometask-server.service";

@Component({
    moduleId: module.id,
    selector: 'marks',
    templateUrl: 'marks.component.html',
    styleUrls: ['marks.component.css']
})
export class MarksComponent implements OnInit {
    @ViewChild('childModal') public childModal:ModalDirective;
    @ViewChild('hometask') public hometask:ModalDirective;
    subjectId: number;
    subject: Subject;
    hometasks: Hometask[];
    students: Student[];
    marks: HometaskStudentMark[];
    constructor(private router: Router,private route: ActivatedRoute, private location: Location, private subjectService : TeacherSubjectServerService, private hometaskService : HometaskServerService) {
        this.subject = new Subject(0, '');
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.subjectId = id;
            this.refreshData();
        });
    }

    getSum(student: Student) {
        let sum = this.marks.reduce((prev, cur, i, a) => {
            if(cur.studentId == student.id)
                return prev + cur.mark;
            else
                return prev;
        }, 0);
        sum = sum == Number.NEGATIVE_INFINITY ? 0 :
            sum == Number.POSITIVE_INFINITY ? 0 : sum;

        return  sum.toString();
    }

    refreshData() {
        this.getSubject()
            .then(() => this.getHometasks())
            .then(() => this.getStudents())
            .then(() => {
                this.marks = [];
                this.students.forEach(s => {
                    this.subjectService.getTopMarks(s, this.subject).then((m) => {
                        let i = 0;
                        m.forEach((mark) => {
                            this.marks.push({
                                mark : mark,
                                studentId : s.id,
                                hometaskId : this.hometasks[i].id
                            });
                            i += 1;
                        })
                    });
                })
            });
    }

    getSubject(): Promise<void> {
        return this.subjectService.getSubjectById(this.subjectId).then(s => {
            console.log(s)
            console.log(this.subjectId)
                this.subject = s;
        });
    }

    getHometasks(): Promise<void> {
        return this.hometaskService.getHometasks(this.subject.id)
            .then(hs => {this.hometasks = hs;});
    }

    private getStudents(): Promise<void> {
        return this.subjectService.getStudents(this.subject).then(ns => {this.students = ns;});
    }

    public getTopMark(student : Student, hometask: Hometask) : string{
        let mark = this.marks.find((m) => m.studentId == student.id && m.hometaskId == hometask.id);

        if(mark == null || mark == undefined)
            return '';
        if(mark.mark == null || mark.mark == undefined || mark.mark <=0)
            return '';

        return mark.mark.toString();

    }

    gotoSubjects() {
        this.router.navigateByUrl('/subjects')
    }


}

class HometaskStudentMark {
    hometaskId: number;
    studentId: string;
    mark: number
}