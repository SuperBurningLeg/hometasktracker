/**
 * Created by kostya on 12/14/2016.
 */
import {Component} from "@angular/core";
import {SettingsServerService} from "../../services/settings-server.service";
@Component({
    moduleId: module.id,
    selector: 'settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css']
})
export class SettingsComponent {
    name: string;
    inviteEmail: string;
    errorMessage: string;
    success: boolean;
    constructor(private settingsService : SettingsServerService) {

    }

    invite() {
        this.settingsService.inviteTeacher(this.inviteEmail).then(() => this.success = true).catch(e => this.errorMessage = e);
    }

}