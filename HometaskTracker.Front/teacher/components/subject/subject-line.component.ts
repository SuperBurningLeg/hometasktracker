/**
 * Created by kostya on 12/11/2016.
 */
import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Subject} from "../../subject";
import {TeacherSubjectServerService} from "../../services/subject-server.service";
import {SubscribedSubject} from "../../../student/subscribed-subject";
import {Router} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'subject-line',
    templateUrl: 'subject-line.component.html',
    styleUrls: ['subject-line.component.css']

})
export class SubjectLineComponent {
    @Input()
    subject: Subject;
    pending: SubscribedSubject[];
    isPendingOpened: boolean;
    isChangeOpened: boolean;
    newName: string;
    @Output()
    onError = new EventEmitter<string>();
    @Output() onChanged = new EventEmitter();

    @Output()
    onDelete = new EventEmitter();

    constructor(private router: Router, private subjectService : TeacherSubjectServerService) {
    }

    openPending() {
        this.getPending();
        this.isPendingOpened = true;
    }

    closePending() {
        this.isPendingOpened = false;
    }


    getPending() {
        this.subjectService.getPending(this.subject).then(pending => {
                this.pending = pending;
        })
    }

    deleteSubject() {
        this.subjectService.deleteSubject(this.subject).then(() => this.onChanged.emit())
            .catch((e) => this.onError.emit(e));
        this.onDelete.emit();
        return false;
    }

    openChangeSubject() {
        this.newName = this.subject.name
        this.isChangeOpened = true;
    }

    closeChangeSubject() {
        this.newName = ""
        this.isChangeOpened = false;
    }

    acceptStudent(sub: SubscribedSubject) {
        this.subjectService.acceptStudent(sub).then(() => this.isPendingOpened = false);
    }

    changeSubject() {
        this.subject.name = this.newName;
        this.subjectService.changeSubject(this.subject).then(() => {
            this.onChanged.emit();
            this.isChangeOpened = false
        })
            .catch((e) => this.onError.emit(e));

        // To prevent default
        return false;
    }


    navigateToSubject(id: number) {
        let url: string = '/subjects/' + id + '/hometasks(commits:' + id + ';)';
        this.router.navigateByUrl(url)
    }
    navigateToMarks(id: number) {
        let url: string = '/subjects/' + id + '/marks(commits:' + id + ';)';
        this.router.navigateByUrl(url)
    }

}