import {Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';

import {SubjectService} from './../../services/subject.service';
import {TeacherSubjectServerService} from './../../services/subject-server.service';
import {Subject} from './../../subject';
import {Subscription} from 'rxjs';
import {SubscribedSubject} from "../../../student/subscribed-subject";
import {Router, Params} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'subject',
    templateUrl: 'subject.component.html',
    styleUrls: ['subject.component.css'],
    animations: [
        trigger('toggleState', [
            state('inactive', style({
                backgroundColor: '#eee',
                transform: 'scale(1)'
            })),
            state('active',   style({
                backgroundColor: '#cfd8dc',
                transform: 'scale(1.1)'
            })),
            transition('inactive => active', animate('100ms ease-in')),
            transition('active => inactive', animate('100ms ease-out'))
        ])
    ]
})

export class TeacherSubjectComponent implements OnInit {
    title: string = "Subjects";
    
    changeToggledId:number;
    
    constructor(private subjectService : TeacherSubjectServerService, private router: Router) {
        this.subjectToAdd = null;
        this.changeToggledId = -1;
        this.errorMessage = null;
        this.subjects = []
        
     }
    subjectToAdd: Subject;
    errorMessage : string;
    subjects : Subject[];

    ngOnInit() {
        console.log(this.subjectService);
        this.getSubjects();
    }

    getSubjects(){
        return this.subjectService.getSubjects()
                .subscribe(subjects => this.subjects = subjects, 
                            error => this.errorMessage = error);
    }

    createSubject(subject: Subject) {
        this.subjectService.createSubject(subject).then(() => {this.getSubjects(); this.untoggleToAdd();})
                                                .catch((e) => this.errorMessage = e.message);

        // To prevent default
        return false;
    }

    toggleToAdd()
    {
        this.subjectToAdd = new Subject(0, "");
    }

    untoggleToAdd() {
        this.subjectToAdd = null;
    }
}