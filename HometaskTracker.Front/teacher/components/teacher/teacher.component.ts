import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import 'teacher/rxjs-operators';
import {Location} from '@angular/common'
import {HometaskCommit} from "../../hometask-commit";
import {TeacherSubjectServerService} from "../../services/subject-server.service";
@Component({
    moduleId: module.id,
    selector: 'teacher',
    templateUrl: 'teacher.component.html',
    styleUrls: ['teacher.component.css']
}) 

export class TeacherComponent implements OnInit {
    constructor(private router: Router, private subjectService : TeacherSubjectServerService, private location: Location) {
    }
    title = "Teacher Component";

    ngOnInit() {
        this.router.navigateByUrl('/subjects');
    }

    goToSubjects()
    {
        this.router.navigateByUrl('/subjects');
    }

    goToSettings() {
        this.router.navigateByUrl('/settings');
    }
    goBack() {
        this.location.back();
    }


}