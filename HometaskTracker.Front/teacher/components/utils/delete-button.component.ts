/**
 * Created by kostya on 12/11/2016.
 */
import {Component, ViewChild, EventEmitter, Output} from "@angular/core";
import {ModalDirective} from "ng2-bootstrap";
@Component({
    moduleId: module.id,
    selector: 'delete-button',
    templateUrl: 'delete-button.component.html'
})
export class DeleteButtonComponent {
    @ViewChild('popup') public popup:ModalDirective;
    @Output()
    onDelete = new EventEmitter;
    popModal() {
        this.popup.show();
    }

    unpopModal(toDelete: boolean) {
        if(toDelete)
            this.onDelete.emit();
        this.popup.hide();
    }
}