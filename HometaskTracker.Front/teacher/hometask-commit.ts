/**
 * Created by kostya on 11/18/2016.
 */
export class HometaskCommit {
    hometaskId: number;
    hometaskName: string;
    subjectName: string;
    commitId: number;
    timeStamp: Date;
    studentName: string;
    studentId: string;
    url: string;
    mark: number;
    maxMark: number;
    message
}