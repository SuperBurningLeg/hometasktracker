import {Subject} from './subject'

export class Hometask {
    subjectId: number;
    id: number;
    deadline: Date;
    description: string;
    name: string;
    repositoryPath: string;
    maxMark: string;
}