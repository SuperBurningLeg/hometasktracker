import {Subject} from './../subject';
import { Observable }     from 'rxjs/Observable';

export interface ISubjectService {
    getSubjects() : Observable<Subject[]>;
    createSubject(subjectToAdd : Subject) : boolean;
    getSubjectById(id: number): Subject;
    changeSubject(subject: Subject): boolean;
    deleteSubject(subject: Subject): boolean;
}