import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Subject} from "../subject";
import {Injectable} from "@angular/core";
import {HometaskCommit} from "../hometask-commit";
import {toPromise} from "rxjs/operator/toPromise";
import {Hometask} from "../hometask";
import {Student} from "../../student/student";
/**
 * Created by kostya on 12/7/2016.
 */
@Injectable()
export class CommitsServerService {
    readonly serverAddress: string = "/";
    readonly commitsURI: string = "API/Teacher/Commits";

    constructor(private http: Http) {

    }

    getLastCommits(subject: Subject) : Promise<HometaskCommit[]> {

        return this.http.get(this.serverAddress + this.commitsURI + "/GetLastCommitsBySubject?subjectId=" + subject.id).toPromise()
            .then(this.handleCommits);
    }
    getLastCommitsByHometask(subjectId: number, hometask: Hometask) : Promise<HometaskCommit[]> {

        return this.http.get(this.serverAddress + this.commitsURI + "/GetLastCommitsByHometask?subjectId=" + subjectId + '&hometaskId=' + hometask.id).toPromise()
            .then(this.handleCommits);
    }

    handleCommits(res: Response): HometaskCommit[] {
        let commits: HometaskCommit[] = res.json() || {};

        return commits;
    }

    addMark(commit: HometaskCommit) : Promise<void> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serverAddress + this.commitsURI + "/AddMark", JSON.stringify(commit), options).toPromise().then(() => {return;}).catch(
            (res : Response) => {
                throw res.json().message;
            }
        );
    }

    getTopMarks(student: Student, subject: Subject) {
        return this.http.get(this.serverAddress + this.commitsURI + "/GetTopMarks?studentId=" + student.id + "&subjectId=" + subject.id).toPromise()
            .then((res: Response) => res.json());
    }
}