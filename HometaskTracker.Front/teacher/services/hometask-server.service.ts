import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Injectable }     from '@angular/core';

import {Hometask} from './../hometask';


@Injectable()
export class HometaskServerService {
    readonly serverAddress: string = "/";
    readonly hometasksAdress = "API/Teacher/Hometasks";
    readonly subjectsOption: string = "?subjectId=";

    constructor(private http: Http) {

    }
        
    getHometasks(subjectId: number) : Promise<Hometask[]> {
        return this.http.get(this.serverAddress + this.hometasksAdress + this.subjectsOption + subjectId).toPromise()
                .then(this.extractHometasks)
                .catch(this.handleError);
    }
    extractHometasks(res: Response) : Hometask[] {
        let hometasks = res.json();
        return hometasks;
    }

    handleError(error: Response | any) {
        error = error.json()
        if(error.message == undefined)
            throw new Error('Error getting data from the server');
        else if(error.code != 200)
            throw new Error(error.message);
    }

    createHometask(hometask : Hometask) : Promise<boolean> {
        hometask.id = 0;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serverAddress + this.hometasksAdress, JSON.stringify(hometask), options)
            .toPromise().catch(this.handleError).then(this.handleCreateSubjectResponse);
    }

    handleCreateSubjectResponse(res: Response){
        let subject : {
            subjectId: number, hometask: Hometask
        } & any = res.json();
        if(subject.subjectId == undefined || subject.subjectId == null)
            throw new Error(subject.message);
        return true;
    }

    changeHometask(hometask : Hometask) {
                let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put(this.serverAddress + this.hometasksAdress, JSON.stringify(hometask), options).toPromise()
                .then(this.handleError)
                .catch(this.handleError);
    }

    deleteHometask(hometask : Hometask) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({
            headers : headers,
            body : JSON.stringify(hometask)
        });
        return this.http.delete(this.serverAddress + this.hometasksAdress, options).toPromise()
                .then(this.handleError)
                .catch(this.handleError);
    }
 

}