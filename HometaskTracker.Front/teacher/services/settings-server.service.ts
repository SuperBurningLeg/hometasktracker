/**
 * Created by kostya on 12/15/2016.
 */
import {Injectable} from "@angular/core";
import {Http, RequestOptions, Headers} from "@angular/http";
@Injectable()
export class SettingsServerService {
    constructor(private http: Http) {

    }

    inviteTeacher(email: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("/api/teacher/settings/inviteteacher", JSON.stringify(email), options).toPromise().catch(res => {throw new Error(res.json().message)});
    }
}