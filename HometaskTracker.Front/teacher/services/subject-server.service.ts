import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Injectable }     from '@angular/core';
import { Observable, ObservableInput }     from 'rxjs/Observable';

import {Subject} from './../subject';

import './../rxjs-operators';
import {SubscribedSubject} from "../../student/subscribed-subject";
import {HometaskCommit} from "../hometask-commit";
import {Student} from "../../student/student";

@Injectable()
export class TeacherSubjectServerService {
    readonly serverAddress: string = "/";
    readonly subjectsURI: string = "API/Teacher/Subjects"; 
    private subjects: Subject[];
    readonly studentsActionWithParam : string = "/Students?subjectId="
    constructor(private http: Http) {

    }

    getSubjects() : Observable<Subject[]> {
        return this.http.get(this.serverAddress + this.subjectsURI)
                .map(this.extractSubjects)
                .catch(this.handleError);
    }

    handleError(error: Response | any): ObservableInput<Subject[]>{
        throw new Error('Error getting data from the server');
    }

    extractSubjects(res: Response) : Subject[]{
        let subjects = res.json() || {};
        return subjects ;
    }

    createSubject(subjectToAdd : Subject) : Promise<boolean> {
                let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serverAddress + this.subjectsURI, JSON.stringify(subjectToAdd), options).toPromise()
                .then(this.handleCreateSubjectResponse);
                //.catch(() => {throw new Error('Error connecting to the server.');});
    }

    handleCreateSubjectResponse(res: Response) {
        let subject : Subject & any = res.json();
        if(subject.id == undefined || subject.id == null)
            throw new Error(subject.message);
        return true;
    }

    handleCodeSubjectResponse(res: Response): boolean {
        let message = res.json();

        if(message.code == 200)
            return true;

        throw new Error(message.message);
    }


    getSubjectById(id: number): Promise<Subject>{
        return this.getSubjects().toPromise()
                                .then(subjects => subjects.find(subject => subject.id == id));
    }


    changeSubject(subject: Subject): Promise<boolean>{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put(this.serverAddress + this.subjectsURI, JSON.stringify(subject),options)
                .toPromise()
                .then(this.handleCodeSubjectResponse);
    }
    deleteSubject(subject: Subject): Promise<boolean>{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({
            headers: headers,
            body : JSON.stringify(subject)
        });
        console.log(JSON.stringify(subject));
        return this.http.delete(this.serverAddress + this.subjectsURI, options).toPromise()
                .then(this.handleCodeSubjectResponse);
    }
    getPending(subject: Subject) : Promise<SubscribedSubject[]> {
        return this.http.get(this.serverAddress + this.subjectsURI + "/PendingStudents?subjectId=" + subject.id.toString()).toPromise()
            .then(this.handlePending);
    }

    handlePending(res: Response): SubscribedSubject[] {
        let subjects : SubscribedSubject[] = res.json() || {};
        return subjects;
    }

    acceptStudent(sub : SubscribedSubject) : Promise<void> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serverAddress + this.subjectsURI + "/AcceptStudent", JSON.stringify(sub), options).toPromise().then(() => {return;});
    }

    getStudents(subject: Subject): Promise<Student[]> {
        return this.http.get(this.serverAddress + this.subjectsURI + this.studentsActionWithParam + subject.id.toString()).toPromise()
            .then(this.extractStudents);
    }

    private extractStudents(res: Response) : Student[] {
        let students = res.json() || {};
        return students;
    }

    getTopMarks(student: Student, subject: Subject) : Promise<number[]>{
        return this.http.get(this.serverAddress + this.subjectsURI + "/GetTopMarks?studentId=" + student.id + "&subjectId=" + subject.id).toPromise()
            .then((res: Response) => res.json());
    }


}