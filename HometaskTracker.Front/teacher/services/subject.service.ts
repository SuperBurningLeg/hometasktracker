
import { Observable, ObservableInput }     from 'rxjs/Observable';
import {Injectable} from '@angular/core';

import {Subject} from './../subject';

import {SUBJECTS} from './../subject-mock';
import {ISubjectService} from './ISubject-service';

Injectable()
export class SubjectService {
    private subjects: Subject[];

    constructor() {
        this.subjects = SUBJECTS;
    }

    private findSubjectByName(subjectName: string) : Subject {
        return this.subjects.find(s => s.name == subjectName);
    } 

    private findSubjectById(subjectId: number) : Subject {
        return this.subjects.find(s => s.id == subjectId);
    }

    getSubjects(): Subject[] {
        return this.subjects
    }

    private handleError() : any {
        return Observable.throw('Error');
    }

    createSubject(subjectToAdd : Subject) : boolean {

        if(subjectToAdd == null || subjectToAdd.name == "")
            throw new Error('Subject should not be empty');

        let  sameNameSubject = this.findSubjectByName(subjectToAdd.name);

        if(sameNameSubject != null)
            throw new Error('Subject already exists');

        let newId = this.subjects[this.subjects.length - 1].id + 1;
        subjectToAdd.id = newId; 

        this.subjects.push(subjectToAdd);
        return true;
    }

    getSubjectById(id: number): Subject {
        return this.subjects.find(s => s.id == id);
    }

    changeSubject(subject: Subject): boolean {
        let sameNameSubject = this.findSubjectByName(subject.name);

        if(sameNameSubject != null)
        {
            if(sameNameSubject.id == subject.id)
                return;

            throw new Error('Subject already exists');
        }

        let subjectById = this.subjects.find(s => s.id == subject.id);
        subjectById.name = subject.name;
        return true;
    }

    deleteSubject(subject: Subject): boolean{
        let subjectById : Subject= this.findSubjectById(subject.id);

        if(subjectById == null)
            throw new Error('Subject is not existing');

        let index : number = this.subjects.indexOf(subjectById);

        this.subjects.splice(index, 1);

        return true;
    }
}