import {Subject} from './subject';

export const SUBJECTS: Subject[] =
[
    new Subject(0, "Subject0"),
    new Subject(1, "Subject1"),
    new Subject(2, "Subject2"),
    new Subject(3, "Subject3"),
    new Subject(4, "Subject4"),
    new Subject(5, "Subject5")
] 