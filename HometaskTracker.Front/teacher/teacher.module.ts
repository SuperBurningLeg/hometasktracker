import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import {RouterModule, Routes}   from '@angular/router';

import {TeacherComponent} from './components/teacher/teacher.component';
import {TeacherSubjectComponent} from './components/subject/subject.component';
import {HometaskComponent} from './components/hometask/hometask.component';
import {CommitsComponent} from './components/commits/commits.component';
import {TeacherSubjectServerService} from './services/subject-server.service';
import {HometaskServerService} from './services/hometask-server.service';
import {CommitsServerService} from "./services/commit-server-service";
import {SecretCodeComponent} from "../shared/components/secretcode.component";
import {SubjectLineComponent} from "./components/subject/subject-line.component";
import {HometaskLineComponent} from "./components/hometask/hometask-line.component";
import {HometaskEditorComponent} from "./components/hometask/hometask-editor.component";
import {DeleteButtonComponent} from "./components/utils/delete-button.component";
import {ModalDirective, ModalModule, DatePickerComponent, DatepickerModule} from "ng2-bootstrap";
import {SettingsComponent} from "./components/settings/settings.component";
import {SettingsServerService} from "./services/settings-server.service";
import {StudentModule} from "../student/student.module";
import {SubjectComponent} from "../student/components/subject/subject.component";
import {SubjectServerService} from "../student/services/subject-server.service";
import {MarksComponent} from "./components/marks/marks.component";
const appRoutes : Routes = [
    {path: 'subjects/:id/hometasks', component: HometaskComponent},
    {path: 'subjects', component: TeacherSubjectComponent},
    {path: '', pathMatch: 'full', redirectTo: '/subjects'},
    {path: ':id', component : CommitsComponent, outlet:"commits"},
    {path: '', component : CommitsComponent, outlet:"commits"},
    {path: 'settings', component: SettingsComponent},
    {path: 'subjects/:id/marks', component: MarksComponent}


];

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, JsonpModule,
     RouterModule.forRoot(appRoutes), ModalModule, DatepickerModule],
    declarations: [TeacherComponent, TeacherSubjectComponent, HometaskComponent, CommitsComponent, SecretCodeComponent,
        SubjectLineComponent, HometaskLineComponent, HometaskEditorComponent, DeleteButtonComponent, SettingsComponent, MarksComponent],
    bootstrap: [TeacherComponent],
    providers: [TeacherSubjectServerService, HometaskServerService, CommitsServerService, SettingsServerService, SubjectServerService]
})
// SubjectService, 
export class TeacherModule { }

