﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Controllers.Student;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Xunit;

namespace HometaskTracker.Tests.API.Student
{
    /*public class SubjectsControllerTest
    {
        // TODO : TEST THIS SHIT LIKE WTF 
        // INTEGRATION TESTS ??? 
        // TEST DATABASE ???
        private SubjectsController GetSubjectsController()
        {
            var subjects = new List<Subject>
            {
                new Subject {SubjectId = 1},
                new Subject {SubjectId = 2},
                new Subject {SubjectId = 3}
            };
            var user = new User
            {
                Id = "1"
            };

            var studentSubjects = new List<StudentSubject>
            {
                new StudentSubject
                {
                    StudentId = "1",
                    SubjectId = 3
                },
                new StudentSubject()
                {
                    StudentId = "2",
                    SubjectId = 2
                },
                new StudentSubject
                {
                    StudentId   = "1",
                    SubjectId = 1
                }
            }.AsQueryable();

            var userStore = new Mock<IUserStore<User>>();
            var userManager = new Mock<UserManager<User>>(userStore.Object);
            userManager.Setup(u => u.GetUserAsync(It.IsAny<ClaimsPrincipal>())).Returns(Task.FromResult(user));

            var subjectRepository = new Mock<ISubjectRepository>();
            subjectRepository.Setup(s => s.Subjects).Returns(() => subjects.AsQueryable());

            var dbSetMock = new Mock<DbSet<StudentSubject>>();
            dbSetMock.As<IQueryable<StudentSubject>>().Setup(m => m.Provider).Returns(studentSubjects.Provider);
            dbSetMock.As<IQueryable<StudentSubject>>().Setup(m => m.Expression).Returns(studentSubjects.Expression);
            dbSetMock.As<IQueryable<StudentSubject>>().Setup(m => m.ElementType).Returns(studentSubjects.ElementType);
            dbSetMock.As<IQueryable<StudentSubject>>().Setup(m => m.GetEnumerator()).Returns(studentSubjects.GetEnumerator());

            var mockContext = new Mock<EfDbContext>();
            mockContext.Setup(c => c.StudentSubject).Returns(() => dbSetMock.Object);

            var subSubjects = new SubscribedSubjectsRepository(mockContext.Object);
            
            var controller = new SubjectsController(userManager.Object, subjectRepository.Object, subSubjects);
            return controller;
        }

        [Fact]
        public async void GetsSubjscribedSubjects()
        {
            var controller = GetSubjectsController();
            var ss = await controller.Subjects();
            Assert.Equal(ss, new List<SubjectViewModel>
            {
                new SubjectViewModel
                {
                    Id = 3
                },
                new SubjectViewModel
                {
                    Id = 1
                }
            });
        }

    }*/
}
