﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Internal.Networking;
using Microsoft.Extensions.Primitives;

namespace HometaskTracker.Web.Areas.Api.Controllers
{
    [ValidateAntiForgeryToken]
    [Authorize]
    [ApiException]
    [Route("api/[controller]")]
    public class ApiAuthenticationController : Controller
    {
        private UserManager<User> _manager;
        private readonly int codeLength = 10;
        private readonly string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
        public ApiAuthenticationController(UserManager<User> manager)
        {
            _manager = manager;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<string> NewCode()
        {
            var user = await _manager.GetUserAsync(this.User);
            var code = GenerateRandomApiCode(codeLength);
            user.ApiSecret = code;
            await _manager.UpdateAsync(user);
            return user.ApiSecret;
        }

        private string GenerateRandomApiCode(int length)
        {
            char[] chars = new char[length];
            Random r = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = alphabet[r.Next(alphabet.Length)];
            }
            return new string(chars);
        }
    }
}
