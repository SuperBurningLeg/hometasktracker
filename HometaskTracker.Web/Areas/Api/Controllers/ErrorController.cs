﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace HometaskTracker.Web.Areas.Api.Controllers
{
    [ValidateAntiForgeryToken]
    [Area("Api")]
    public class ErrorController : Controller
    {
        public IActionResult Error(ErrorInformation info)
        {
            return new JsonResult(new {Error= info});
        }

        public IActionResult NotAuthorized()
        {
            var errorInfo = new ErrorInformation()
            {
                Code = (int) HttpStatusCode.Unauthorized,
                Message = "You are not authorized"
            };

            return Error(errorInfo);
        }
    }
}
