﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Areas.Api.Controllers.Student
{
    [Authorize(Policy = "Student")]
    [Route("API/Student/[controller]")]
    [ApiException]
    [ValidateAntiForgeryToken]
    public class CommitsController : Controller
    {
        private readonly CommitRepository _commitsRepository;

        public CommitsController(CommitRepository commitsRepository)
        {
            _commitsRepository = commitsRepository;
        }
        // GET: /<controller>/
        [HttpGet]
        [Route("")]
        public IEnumerable<CommitView> GetCommits(string studentId, int hometaskId)
        {
            var commits = _commitsRepository.GetCommits(studentId, hometaskId);
            return commits.Select(c => c.ToView(hometaskId, studentId));
        }

        [HttpGet]
        [Route("[action]")]
        public IEnumerable<double> GetTopMarks(string studentId,
            int subjectId)
        {
            return _commitsRepository.GetTopMarksCommits(studentId, subjectId);
        }
    }
}
