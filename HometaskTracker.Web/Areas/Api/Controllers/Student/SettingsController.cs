﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Areas.Api.Controllers.Student
{
    [Authorize(Policy = "Student")]
    [ValidateAntiForgeryToken]
    [Route("API/Student/[controller]")]
    [ApiException]
    public class SettingsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ISubjectSubscriber _subscriber;
        private readonly ISubjectService _subjectService;
        private readonly SubscribedSubjectsRepository _subscribedSubjectsRepository;
        private readonly SignInManager<User> _signInManager;

        public SettingsController(UserManager<User> userManager, ISubjectSubscriber subscriber, ISubjectService subjectService,
            SubscribedSubjectsRepository subscribedSubjectsRepository, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _subscriber = subscriber;
            _subjectService = subjectService;
            _subscribedSubjectsRepository = subscribedSubjectsRepository;
            _signInManager = signInManager;
        }

        // GET: /<controller>/
        [HttpPost]
        [Route("[action]")]
        public async Task<ErrorInformation> SubscribeToSubject([FromBody] SubjectViewModel view)
        {
            var userId = _userManager.GetUserId(this.User);
            try
            {
                await _subscriber.SubscribeToSubject(view.Id, userId);
            }
            catch (ArgumentException e)
            {
                return new ErrorInformation{Code=400, Message = e.Message};
            }

            return ErrorInformationFactory.Ok();
        }

        [HttpDelete]
        [Route("SubscribeToSubject")]
        public async Task<ErrorInformation> UnsubscribeFromSubject([FromBody] SubjectSubscriptionView view)
        {
            var userId = _userManager.GetUserId(this.User);
            try
            {
                await _subscriber.UnsubscribeFromSubject(view.SubjectId, userId);
            }
            catch (ArgumentException e)
            {
                return new ErrorInformation { Code = 400, Message = e.Message };
            }

            return ErrorInformationFactory.Ok();
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<SubjectSubscriptionView>> SubscribedSubjects()
        {
            var user = await _userManager.GetUserAsync(this.User);
            return user.StudentSubjects.Select(ss => ss.ToSubscriptionView());
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<SubjectViewModel>> Subjects()
        {
            var subscribed = await SubscribedSubjects();
            return _subjectService.Subjects.Where(s => subscribed.FirstOrDefault(ss => ss.SubjectId == s.SubjectId) == null).Select(s => s.ToView());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ErrorInformation> Webhook([FromBody]SubjectSubscriptionView view)
        {
            var user = await _userManager.GetUserAsync(this.User);
            _subscribedSubjectsRepository.User = user;
            await HttpContext.Session.LoadAsync();
            var token = HttpContext.Session.GetString("token");
            if (token == null)
            {
                await _signInManager.SignOutAsync();
                return ErrorInformationFactory.Bad();
            }
            _subscribedSubjectsRepository.User = user;
            var adder = new WebhookAdder(user, _subscribedSubjectsRepository);
            var result = await adder.AddWebhook(view.SubjectId, view.RepositoryName, token);
            if (result)
            {
                await _subscribedSubjectsRepository.SetRepository(view.SubjectId, view.RepositoryName);
                return ErrorInformationFactory.Ok();
            }

            return ErrorInformationFactory.Bad();
        }
    }
}
