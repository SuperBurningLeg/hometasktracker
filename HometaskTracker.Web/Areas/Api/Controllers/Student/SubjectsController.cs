﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using NuGet.Protocol.Core.v3;

namespace HometaskTracker.Web.Areas.Api.Controllers.Student
{
    [ValidateAntiForgeryToken]
    [Authorize(Policy = "Student")]
    [Route("API/Student")]
    [ApiException]
    public class SubjectsController : Controller
    {
        private UserManager<User> _userManager;
        private ISubjectService _subjectService;
        private readonly SubscribedSubjectsRepository _subscribedSubjectsRepository;

        public SubjectsController(UserManager<User> userManager,
            ISubjectService subjectService, SubscribedSubjectsRepository subscribedSubjectsRepository)
        {
            _userManager = userManager;
            _subjectService = subjectService;
            _subscribedSubjectsRepository = subscribedSubjectsRepository;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<SubjectViewModel>> Subjects()
        {
            var user = await _userManager.GetUserAsync(this.User);
            _subscribedSubjectsRepository.User = user;
            return _subscribedSubjectsRepository.GetSubscribedSubjects().Select(s => s.ToView());
            //return user.ToJson();
        }

        [HttpGet]
        [Route("Hometasks")]
        public async Task<IEnumerable<HometaskView>> GetHometasks(int subjectId)
        {
            var user = await _userManager.GetUserAsync(this.User);
            _subscribedSubjectsRepository.User = user;
            return _subscribedSubjectsRepository.GetHometasks(subjectId).Select(h => h.ToView());
        }

        [HttpGet]
        [Route("Students")]
        public async Task<IEnumerable<StudentView>> GetStudents(int subjectId)
        {
            var user = await _userManager.GetUserAsync(this.User);
            _subscribedSubjectsRepository.User = user;
            return _subscribedSubjectsRepository.GetStudentsSubscribedTo(subjectId).Select(s => s.ToView());
        }
    }
}
