﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Areas.Api.Controllers.Teacher
{
    [ValidateAntiForgeryToken]
    [Route("Api/Teacher/[controller]")]
    [ApiException]
    [Authorize(Policy = "Teacher")]
    public class CommitsController : Controller
    {
        private readonly CommitRepository _repo;
        public CommitsController(CommitRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Route("[action]")]
        public IEnumerable<HometaskCommitView> GetLastCommitsBySubject(int subjectId)
        {
            var commits = _repo.GetLastCommits(subjectId).Select(hc => hc.ToView());
            return commits;
        }
        [HttpGet]
        [Route("[action]")]
        public IEnumerable<HometaskCommitView> GetLastCommitsByHometask(int subjectId, int hometaskId)
        {
            return _repo.GetLastCommits(subjectId, hometaskId).Select(hc => hc.ToView());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ErrorInformation> AddMark([FromBody]HometaskCommitView view)
        {
            await _repo.AddMark(view.CommitId, view.StudentId, view.HometaskId, view.Mark, view.Message);
            return ErrorInformationFactory.Ok();
        }
    }
}
