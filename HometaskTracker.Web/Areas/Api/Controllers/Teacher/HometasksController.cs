﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Helpers;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Areas.Api.Controllers.Teacher
{
    [ValidateAntiForgeryToken]
    [Authorize(Policy = "Teacher")]
    [Route("API/Teacher/[controller]")]
    [ApiException]
    public class HometasksController : Controller
    {
        private ISubjectService _repo;
        public HometasksController(ISubjectService repo)
        {
            _repo = repo;
        }

        // GET: /<controller>/
        [HttpGet]
        [Route("")]
        public async Task<IEnumerable<HometaskView>> Hometasks(int subjectId)
        {
            var subject = await _repo.GetSubjectByIdAsync(subjectId);
            
            if(subject == null)
                throw new ArgumentException("Could not find the subject");

            return subject.Hometasks.Select(h => h.ToView());
        }

        [HttpPost]
        [Route("")]
        public async Task<HometaskView> HometaskNew([FromBody]HometaskView view)
        {
            if (view == null)
                throw new ArgumentException("Could not parse the hometask");

            var subject = await _repo.GetSubjectByIdAsync(view.SubjectId);

            if (subject == null)
                throw new ArgumentException("Could not find the subject");
            view.Id = 0;
            _repo.AddHometask(subject, view.ToHometask());

            return view;
        }



        [HttpPut]
        [Route("")]
        public ErrorInformation HometasksChange([FromBody]HometaskView view)
        {
            if (view == null)
                return new ErrorInformation { Code = StatusCodes.Status400BadRequest, Message = "Could not parse the hometask" };

            if (_repo.ChangeHometask(view.ToHometask()))
                return ErrorInformationFactory.Ok();

            return ErrorInformationFactory.Bad();
        }

        [HttpDelete]
        [Route("")]
        public ErrorInformation HometasksDelete([FromBody]HometaskView view)
        {
            if (view == null)
                return new ErrorInformation { Code = StatusCodes.Status400BadRequest, Message = "Could not parse the hometask" };

            if (_repo.DeleteHometask(view.ToHometask()))
                return ErrorInformationFactory.Ok();

            return ErrorInformationFactory.Bad();
        }
    }
}
