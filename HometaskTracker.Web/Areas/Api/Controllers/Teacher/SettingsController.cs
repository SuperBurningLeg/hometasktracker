﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Template;

namespace HometaskTracker.Web.Areas.Api.Controllers.Teacher
{
    [ValidateAntiForgeryToken]
    [Route("API/Teacher/[controller]")]
    [ApiException]
    [Authorize(Policy = "Teacher")]
    public class SettingsController : Controller
    {
        private readonly TeacherInviter _inviter;

        public SettingsController(TeacherInviter inviter)
        {
            _inviter = inviter;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> InviteTeacher([FromBody]string email)
        {
            if(email == null)
                throw new NullReferenceException("Email was null");
            try
            {
                await _inviter.InviteTeacher(new MailAddress(email));
            }
            catch (FormatException)
            {
                throw new ArgumentException("Could not parse email");
            }

            return Ok();
        }
    }
}
