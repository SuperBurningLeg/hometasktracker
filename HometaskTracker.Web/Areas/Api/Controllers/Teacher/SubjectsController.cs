﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Extensions;
using HometaskTracker.Web.Areas.Api.Helpers;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Protocol.Core.v3;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Areas.Api.Controllers.Teacher
{
    [ValidateAntiForgeryToken]
    [Route("API/Teacher/[controller]")]
    [Authorize(Policy = "Teacher")]
    [ApiException]
    public class SubjectsController : Controller
    {
        private ISubjectService _repo;
        private readonly ISubjectSubscriber _subscriber;
        private readonly UserManager<User> _manager;
        private readonly SubscribedSubjectsRepository _subscribedSubjectsRepository;
        private CommitRepository _commitsRepository;

        public SubjectsController(ISubjectService repo, ISubjectSubscriber subscriber, UserManager<User> manager,
            SubscribedSubjectsRepository subscribedSubjectsRepository,
            CommitRepository commitsRepository)
        {
            this._repo = repo;
            _subscriber = subscriber;
            this._manager = manager;
            _subscribedSubjectsRepository = subscribedSubjectsRepository;
            _commitsRepository = commitsRepository;
        }

        [HttpGet]
        [Route("")]
        public async Task<IEnumerable<SubjectViewModel>> Subjects()
        {
            return await FromSubjectsToView();
        }

        [HttpPost]
        [Route("")]
        public async Task<SubjectViewModel> SubjectsPost([FromBody]SubjectViewModel model)
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            int id;
            Subject s = model.ToSubject();
            try
            {
                id = _repo.CreateSubject(s);
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException("Could not add a subject to database");
            }
            return new SubjectViewModel(id, model.Name);
        }

        [HttpDelete]
        [Route("")]
        public async Task<ErrorInformation> SubjectsDelete([FromBody]SubjectViewModel model)
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Subject s = model.ToSubject();
            try
            {
                _repo.DeleteSubject(s);
            }
            catch (ArgumentException e)
            {
                return new ErrorInformation() { Code = StatusCodes.Status404NotFound, Message = "Error. Could not find the subject" };
            }

            return new ErrorInformation() {Code = StatusCodes.Status200OK, Message = "Success." };
        }

        [HttpPut]
        [Route("")]
        public async Task<ErrorInformation> SubjectsPut([FromBody]SubjectViewModel model)
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            if (model == null)
                return new ErrorInformation() { Code = StatusCodes.Status404NotFound, Message = "Error. Could not parse subject" };

            Subject s = model.ToSubject();
            try
            {
                _repo.ChangeSubject(s);
            }
            catch (ArgumentException e)
            {
                return new ErrorInformation() { Code = StatusCodes.Status404NotFound, Message = "Error. Could not find the subject" };
            }

            return new ErrorInformation() { Code = StatusCodes.Status200OK, Message = "Success." };
        }

        private async Task<IEnumerable<SubjectViewModel>> FromSubjectsToView()
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            return _repo.Subjects.ToList().Select(s => s.ToView());
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<SubjectSubscriptionView>> PendingStudents(int subjectId)
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            var subscriptions = await _subscriber.GetPending(subjectId);
            return subscriptions.Select(s => s.ToSubscriptionView());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task AcceptStudent([FromBody]SubjectSubscriptionView view)
        {
            _repo.Teacher = await _manager.GetUserAsync(this.User);
            await _subscriber.AcceptStudent(view.SubjectId, view.StudentId);
        }

        [HttpGet]
        [Route("Students")]
        public IEnumerable<StudentView> GetStudents(int subjectId)
        {
            _subscribedSubjectsRepository.User = null;
            return _subscribedSubjectsRepository.GetStudentsSubscribedTo(subjectId).Select(s => s.ToView());
        }

        [HttpGet]
        [Route("[action]")]
        public IEnumerable<double> GetTopMarks(string studentId,
    int subjectId)
        {
            return _commitsRepository.GetTopMarksCommits(studentId, subjectId);
        }
    }
}
