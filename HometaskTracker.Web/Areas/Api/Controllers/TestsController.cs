using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Web.Areas.Api.Controllers
{
    [Area("Api")]
    public class TestsController : Controller
    {   
        public IActionResult Test()
        {
            return new JsonResult(new {Message="test"});
        }
    }
}