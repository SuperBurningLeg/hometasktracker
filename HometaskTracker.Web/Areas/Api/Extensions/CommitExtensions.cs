﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Razor.Tokenizer;

namespace HometaskTracker.Web.Areas.Api.Extensions
{
    public static class CommitExtensions
    {
        public static CommitView ToView(this Commit commit, int hometaskId, string studentId)
        {
            return new CommitView
            {
                Id = commit.Id,
                TimeStamp = commit.TimeStamp,
                Mark = commit.Marks.FirstOrDefault(m => m.HometaskId == hometaskId),

            };
        }
    }
}
