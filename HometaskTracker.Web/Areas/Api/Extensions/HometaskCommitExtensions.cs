﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;

namespace HometaskTracker.Web.Areas.Api.Extensions
{
    public static class HometaskCommitExtensions
    {
        public static HometaskCommitView ToView(this HometaskCommit commit)
        {
            return new HometaskCommitView
            {
                CommitId = commit.Commit.Id,
                HometaskId = commit.Hometask.Id,
                SubjectName = commit.Subject.SubjectName,
                TimeStamp = commit.Commit.TimeStamp,
                HometaskName = commit.Hometask.Name,
                StudentId = commit.Student.Id,
                StudentName = commit.Student.Name,
                Url = commit.Commit.Url,
                Mark = 0,
                MaxMark = commit.Hometask.MaxMark,
                Message = ""
            };
        }
    }
}
