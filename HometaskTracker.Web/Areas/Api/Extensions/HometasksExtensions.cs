﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;

namespace HometaskTracker.Web.Areas.Api.Extensions
{
    public static class HometasksExtensions
    {
        public static HometaskView ToView(this Hometask hometask)
        {
            return new HometaskView
            {
                SubjectId = hometask.SubjectId,
                Deadline = hometask.Deadline,
                Description = hometask.Description,
                Name = hometask.Name,
                Id = hometask.Id,
                RepositoryPath = hometask.RepositoryPath,
                MaxMark = hometask.MaxMark
            };
        }
    }
}
