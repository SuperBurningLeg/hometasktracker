﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;

namespace HometaskTracker.Web.Areas.Api.Extensions
{
    public static class StudentExtenstions
    {
        public static StudentView ToView(this User user)
        {
            return new StudentView
            {
                Id = user.Id,
                Name = user.Name,
                AvatarUrl = user.AvatarUrl
            };
        }
    }
}
