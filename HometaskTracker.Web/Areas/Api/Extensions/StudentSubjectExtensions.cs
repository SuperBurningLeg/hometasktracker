﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;

namespace HometaskTracker.Web.Areas.Api.Extensions
{
    public static class StudentSubjectExtensions
    {
        public static SubjectSubscriptionView ToSubscriptionView(this StudentSubject ss)
        {
            Debug.Assert(ss.SubjectId != null, "ss.SubjectId != null");
            return new SubjectSubscriptionView
            {
                RepositoryName = ss.RepositoryName,
                SubjectId = ss.SubjectId.Value,
                SubscriptionStatus = ss.SubscriptionStatus,
                SubjectName = ss.Subject.SubjectName,
                StudentId = ss.StudentId,
                StudentName = ss.Student.Name
            };
        }
    }
}
