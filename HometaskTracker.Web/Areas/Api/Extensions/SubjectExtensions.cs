﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Razor.Tokenizer;

namespace HometaskTracker.Web.Areas.Api.Controllers
{
    public static class SubjectExtensions
    {
        public static SubjectViewModel ToView(this Subject subject)
        {
            return new SubjectViewModel(subject.SubjectId, subject.SubjectName);
        }
    }
}
