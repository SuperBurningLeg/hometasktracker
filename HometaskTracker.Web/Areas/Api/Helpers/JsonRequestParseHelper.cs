﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace HometaskTracker.Web.Areas.Api.Helpers
{
    // This is now useless
    public static class JsonRequestParseHelper
    {
        public static T Parse<T>(HttpRequest request)
        {
            T model;
            var jsonSerializerSettings = new JsonSerializerSettings { MissingMemberHandling = MissingMemberHandling.Error };

            if (request.Body.CanSeek)
            {
                // Reset the position to zero to read from the beginning.
                request.Body.Position = 0;
            }
            using (var reader = new StreamReader(request.Body, Encoding.UTF8, true, 1024, true))
            {
                var subjectString = reader.ReadToEnd();
                Debug.WriteLine($"I am parsing\n {subjectString} ");
                try
                {
                    
                    model = JsonConvert.DeserializeObject<T>(subjectString, jsonSerializerSettings);

                    // IDK why but here sometimes(PUT shit) gets the whole  REQUEST, not only the content from Request.Body
                }
                catch (JsonSerializationException e1)
                {
                    var o = JsonConvert.DeserializeAnonymousType(subjectString, new { body = "" });
                    try
                    {
                        model = JsonConvert.DeserializeObject<T>(o.body, jsonSerializerSettings);
                    }
                    catch (JsonSerializationException e2)
                    {
                        return default(T);
                    }
                    return model;
                }
            }
            return model;
        }
    }
}
