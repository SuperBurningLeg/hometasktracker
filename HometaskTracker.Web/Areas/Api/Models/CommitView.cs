﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class CommitView
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public Mark Mark { get; set; }
    }
}
