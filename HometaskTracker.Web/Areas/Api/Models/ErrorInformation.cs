using System.Collections.Generic;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class ErrorInformation
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}