﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public static class ErrorInformationFactory
    {
        public static ErrorInformation Ok()
        {
            return new ErrorInformation
            {
                Code = 200,
                Message = "All went ok"
            };
        }

        public static ErrorInformation Bad()
        {
            return new ErrorInformation
            {
                Code = 400,
                Message = "Something went wrong"
            };
        }
    }
}
