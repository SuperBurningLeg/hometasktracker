﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class HometaskCommitView
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public int HometaskId { get; set; }
        public string HometaskName { get; set; }
        public string SubjectName { get; set; }
        public int CommitId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Url { get; set; }
        public double Mark { get; set; }
        public double MaxMark { get; set; }
        public string Message { get; set; }
    }
}
