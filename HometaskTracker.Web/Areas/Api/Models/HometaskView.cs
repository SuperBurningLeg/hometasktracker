﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class HometaskView
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public DateTime Deadline { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string RepositoryPath { get; set; }
        public int MaxMark { get; set; }

        public Hometask ToHometask()
        {
            if (RepositoryPath == null)
                throw new ArgumentException("Repository path cannot be empty");
            if (Name == null)
                throw new ArgumentException("Hometask name cannot be empty");
            if (Deadline == null)
                throw new ArgumentException("Deadline cannot be null");
            return new Hometask
            {
                SubjectId = this.SubjectId,
                Deadline =  Deadline,
                Description = Description,
                Name = Name,
                Id = Id,
                RepositoryPath = proccessRepoPath(RepositoryPath),
                MaxMark = MaxMark
            };
        }

        private string proccessRepoPath(string repoPath)
        {
            
            repoPath = repoPath.Trim();
            PathString path;
            try
            {
                path = new PathString(repoPath);
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException("Path should start with / and be correct", e);
            }
            return path.Value;
        }
    }
}
