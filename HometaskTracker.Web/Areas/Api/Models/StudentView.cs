﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Web.Migrations;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class StudentView
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public string AvatarUrl { get; set; }
    }
}
