﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class SubjectSubscriptionView
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        //public Subject Subject { get; set; }
        public string RepositoryName { get; set; }
        public SubscriptionStatus SubscriptionStatus { get; set; }
    }
}
