﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using Newtonsoft.Json;
using HometaskTracker.Domain;

namespace HometaskTracker.Web.Areas.Api.Models
{
    public class SubjectViewModel
    {
        public SubjectViewModel()
        {
            
        }

        public SubjectViewModel(int subjectId,
            string subjectName)
        {
            this.Id = subjectId;
            this.Name = subjectName;
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public static SubjectViewModel FromSubject(Subject s)
        {
            return new SubjectViewModel(s.SubjectId, s.SubjectName);
        }

        public Subject ToSubject()
        {
            if(Name.IsNullOrEmpty())
                throw new ArgumentException("Subject name cannot be null");
            return new Subject(this.Id, this.Name);
        }
    }
}
