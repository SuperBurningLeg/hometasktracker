﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Logging;
using NuGet.Protocol.Core.v3;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace HometaskTracker.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private SignInManager<User> _signInManager;
        private UserManager<User> _userManager;
        private EfDbContext _dbContext;
        private bool isPersistent = false;
        //private ILogger _logger;

        public AccountController(SignInManager<User> signInManager, UserManager<User> userManager, EfDbContext dbContext)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _dbContext = dbContext;
        }

        public IActionResult AccessDenied()
        {
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> LogIn()
        {
            // Check if is already authenticated using OAUTH before. Redirects to AccessDenied in other case(no calls to GitHub)
            if (this.User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("LogIn");
            }

            return new ChallengeResult("GitHub", new AuthenticationProperties() { RedirectUri = "/Account/ValidateLogIn" });
        }

        public async Task<IActionResult> ValidateLogIn()
        {
            if(_signInManager.IsSignedIn(this.User))
                return Redirect("/angularapp/student");
            var user = await _userManager.GetUserAsync(this.User);
            
            await SetTokenToSession();

            /*var result =
                await
                    _signInManager.ExternalLoginSignInAsync(loginInfo.LoginProvider, loginInfo.ProviderKey, isPersistent);*/
           
            if (user != null)
            {
                await _signInManager.SignInAsync(user, true);
                if (user.IsTeacher.GetValueOrDefault() == true)
                    return Redirect("/angularapp/teacher");
                return Redirect("/angularapp/student");
            }
            //await CreateUser();

            return View("NotRegistered");
        }

        public async Task<IActionResult> Register()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Register");
            }

            return new ChallengeResult("GitHub", new AuthenticationProperties() { RedirectUri = "/Account/RegisterNewAccount" });
        }

        public async Task<IActionResult> RegisterNewAccount()
        {
            var user = await _userManager.GetUserAsync(this.User);
            if (user != null)
                return View("AlreadyRegistered");

            user = new User()
            {
                UserName = User.FindFirstValue(ClaimTypes.Name),
                Email = User.FindFirstValue(ClaimTypes.Email),
                NormalizedEmail = User.FindFirstValue(ClaimTypes.Email).ToUpper()
            };

            await _userManager.CreateAsync(user);
            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Name, user.UserName));

            return RedirectToAction("ValidateLogIn");
        }

        private async Task SetTokenToSession()
        {
            var externalInfo = await this.HttpContext.Authentication.GetAuthenticateInfoAsync("Identity.External");
            var token = externalInfo.Properties.GetTokenValue("access_token");
            this.HttpContext.Session.SetString("token", token);
            await HttpContext.Session.CommitAsync();
        }

        [Authorize]
        public async Task<IActionResult> LogOut()
        {
            // All resets if logging out of github account.
            await _signInManager.SignOutAsync();
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public async Task<IActionResult> FillPersonalData()
        {
            var user = await _userManager.GetUserAsync(this.User);
            var split = user?.Name?.Split(' ');
            if (split != null)
            {
                var name = split[0];
                var surname = split[1];
                return View(new UserPersonalDataView
                {
                    Name = name,
                    Surname = surname
                });
            }
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> FillPersonalData(UserPersonalDataView view)
        {
            var validImageTypes = new string[]
            {
                "image/gif",
                "image/jpeg",
                "image/pjpeg",
                "image/png"
            };

            IFormFile avatar = view.Avatar;

            if (avatar != null && !validImageTypes.Contains(avatar.ContentType))
            {
                ModelState.AddModelError("Avatar", "Please choose either a GIF, JPG or PNG image.");
            }

            if (!ModelState.IsValid)
                return View(view);


            if (avatar != null)
            {
                var dir = "wwwroot/images/avatars";
                var imageUrl = Path.Combine(dir, avatar.FileName);
                avatar.CopyTo(new FileStream(imageUrl, FileMode.Create));
            }

            var user = await _userManager.GetUserAsync(this.User);

            user.Name = view.Name.Trim() + " " + view.Surname.Trim();
            await _userManager.AddClaimAsync(user, new Claim("Name", user.Name));
            if(avatar != null)
                user.AvatarUrl = "/images/avatars/" + avatar.FileName;
            await _userManager.UpdateAsync(user);
            await _signInManager.SignInAsync(await _userManager.GetUserAsync(this.User), true);
            return Redirect("/angularapp/student");
        }
    }
}