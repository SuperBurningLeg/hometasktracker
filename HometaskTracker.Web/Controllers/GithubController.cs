﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Models;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HometaskTracker.Web.Controllers
{
    public class GithubController : Controller
    {
        private readonly CommitRepository _repository;
        private readonly UserManager<User> _userManager;
        private readonly ILogger _logger;
        private string _githubHookApi = "192.30.252.0";

        public GithubController(CommitRepository repository, UserManager<User> userManager, ILogger<GithubController> logger)
        {
            _repository = repository;
            _userManager = userManager;
            _logger = logger;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Payload([FromBody]GithubPushEvent pushEvent)
        {
            var remoteIpAddress = GetRequestIp();
            _logger.LogInformation("Webhook came from ip: " + remoteIpAddress);
            var remoteIpParts = remoteIpAddress.Split('.');
            var githubIpParts = _githubHookApi.Split('.');
            for (int i = 0; i < 3; i++)
            {
                if (remoteIpParts[i] != githubIpParts[i])
                    return NotFound();
            }

            var repoName = pushEvent.Repository.Name;
            var userName = pushEvent.Repository.Owner.Name;
            var user = await _userManager.FindByNameAsync(userName);
            _repository.User = user;

            foreach (var commitGithub in pushEvent.Commits)
            {
                HashSet<string> paths = new HashSet<string>();
                List<CommitChange> changes = new List<CommitChange>();
                paths.UnionWith(commitGithub.Added);
                paths.UnionWith(commitGithub.Modified);
                paths.UnionWith(commitGithub.Removed);
                Commit commit = new Commit
                {
                    TimeStamp =  commitGithub.TimeStamp,
                    Url = commitGithub.Url
                };
                foreach (var path in paths)
                {
                    CommitChange change = new CommitChange
                    {
                        Path = "/" + path
                    };
                    changes.Add(change);
                }
                await _repository.AddCommit(commit, changes, repoName);
            }

            return Ok();
        }

        private string GetRequestIp(bool tryUseXForwardHeader = true)
        {
            string ip = null;

            if (tryUseXForwardHeader)
                ip = HttpContext.Request.Headers["X-Forwarded-For"];

            if (ip.IsNullOrEmpty())
                ip = HttpContext.Request.Headers["REMOTE_ADDR"];

            if (ip.IsNullOrEmpty() && HttpContext.Connection?.RemoteIpAddress != null)
                ip = HttpContext.Connection.RemoteIpAddress.ToString();

            if (ip.IsNullOrEmpty())
                throw new Exception("Unable to determine caller's IP.");

            return ip;
        }
    }
}
