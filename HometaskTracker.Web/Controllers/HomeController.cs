using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.AspNetCore.Identity;

namespace HometaskTracker.Web.Controllers
{
    public class HomeController : Controller
    {
        private UserManager<User> _userManager;
        // private readonly IUsersRepository _repository;

        public HomeController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            ViewData["AppName"] = "app";
            ViewData["IsAuthenticated"] = User.Identity.IsAuthenticated;
            return View();
        }

        [Authorize]
        public async Task<IActionResult> StudentApp()
        {
            var user = await _userManager.GetUserAsync(this.User);
            if (!user.PersonalDataFilled)
                return RedirectToAction("FillPersonalData", "Account");
#if DEBUG
            return new FileContentResult(System.IO.File.ReadAllBytes("../HometaskTracker.Front/student.html"), "text/html");
#endif
#if RELEASE
            return new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/HometaskTracker.Front/student.html"), "text/html");
#endif
        }

        [Authorize(Policy = "Teacher")]
        public IActionResult TeacherApp()
        {
#if DEBUG
            return new FileContentResult(System.IO.File.ReadAllBytes("../HometaskTracker.Front/teacher.html"), "text/html");
#endif
#if RELEASE
            return new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/HometaskTracker.Front/teacher.html"), "text/html");
#endif
        }

        [Authorize]
        public IActionResult AngularApp()
        {
#if DEBUG
            return new FileContentResult(System.IO.File.ReadAllBytes("../HometaskTracker.Front/student.html"), "text/html");
#endif
#if RELEASE
            return new FileContentResult(System.IO.File.ReadAllBytes("wwwroot/HometaskTracker.Front/student.html"), "text/html");
#endif
        }

        public IActionResult NotAuthorized()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
