﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HometaskTracker.Domain.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace HometaskTracker.Web.Controllers
{
    [AllowAnonymous]
    public class TeacherAccountController : Controller
    {
        private readonly TeacherInviter _inviter;

        public TeacherAccountController(TeacherInviter inviter)
        {
            _inviter = inviter;
        }

        [HttpGet]
        public async Task<IActionResult> RegisterByInvite(string email,
            string token)
        {
            if (!await _inviter.CheckToken(token, email))
                return View("IncorrectToken");

            if (this.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("LinkGithub", new {email});
            }

            return new ChallengeResult("GitHub", new AuthenticationProperties() { RedirectUri = "/TeacherAccount/LinkGithub?email=" + email });
        }

        [HttpGet]
        public async Task<IActionResult> LinkGithub(string email)
        {
            var userName = User.FindFirstValue(ClaimTypes.Name);
            await _inviter.AddTeacher(email, userName);
            return RedirectToAction("TeacherApp", "Home");
        }

    }
}
