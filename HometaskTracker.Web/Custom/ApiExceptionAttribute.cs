﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Exceptions;
using HometaskTracker.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace HometaskTracker.Web.Custom
{
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var code = context.Exception is ArgumentException ? 400 : 500;

            if (context.Exception is ArgumentException)
            {
                if (context.Exception is EntityNotFoundException)
                    code = 404;
            }

            var error = new ErrorInformation
            {
                Code = code,
                Message = !exception.Message.IsNullOrEmpty() ? exception.Message : "Unknown error." 
            };

            context.HttpContext.Response.Clear();
            context.HttpContext.Response.StatusCode = error.Code;
            context.Result = new JsonResult(error);
        }
    }
}
