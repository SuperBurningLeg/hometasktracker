﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HometaskTracker.Web.Custom
{
    public class AppClaimsPrincipleFactory : IUserClaimsPrincipalFactory<User>
    {
        public Task<ClaimsPrincipal> CreateAsync(User user)
        {
            
            return Task.Factory.StartNew(() =>
            {
                
                var identity = new ClaimsIdentity();
                identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
                identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
                //user.
                identity.AddClaim(new Claim("IsTeacher", user.IsTeacher.GetValueOrDefault().ToString()));
                identity.AddClaim(new Claim("PersonalDataFilled", user.PersonalDataFilled.ToString()));
                var principle = new ClaimsPrincipal(identity);
                
                return principle;
            });
        }
    }

    public class AppUserManager : UserManager<User>
    {
        public AppUserManager(IUserStore<User> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<User>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }

        public  override async Task<User> GetUserAsync(ClaimsPrincipal user)
        {
            var name = user.Identities.FirstOrDefault().FindFirst(ClaimTypes.Name).Value.ToUpper();
            var retUser = await this.Store.FindByNameAsync(name, CancellationToken.None);

            return retUser;
        }

        public override string GetUserId(ClaimsPrincipal user)
        {
            var name = user.Identities.FirstOrDefault().FindFirst(ClaimTypes.Name).Value.ToUpper();
            var retUser = this.Store.FindByNameAsync(name, CancellationToken.None).GetAwaiter().GetResult();

            return retUser.Id;
        }
    }
}
