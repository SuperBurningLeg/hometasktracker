﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace HometaskTracker.Web.Custom
{
    public class AppUserStore : UserStore<User>
    {
        public AppUserStore(IdentityDbContext<User> context) : base(context)
        {

        }

        public override Task<string> GetUserIdAsync(User user,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(user.Id);
        }

        public override Task<User> FindByIdAsync(string userId,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return Users.Include(u => u.StudentSubjects)
                .ThenInclude(ss => ss.Subject)
                .ThenInclude(s => s.Hometasks)
                .Where(u => u.Id == userId)
                .FirstOrDefaultAsync(cancellationToken: cancellationToken);
        }

        public override Task<User> FindByNameAsync(string userName,
    CancellationToken cancellationToken = new CancellationToken())
        {
            return Users.Include(u => u.StudentSubjects)
                .ThenInclude(ss => ss.Subject)
                .ThenInclude(s => s.Hometasks)
                .Where(u => u.UserName == userName)
                .FirstOrDefaultAsync(cancellationToken: cancellationToken);
        }
    }
}
