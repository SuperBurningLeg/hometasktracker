﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HometaskTracker.Web.Custom
{
    public class StudentRequirement : IAuthorizationRequirement
    {
        

    }
    public class StudentRequirementHandler : AuthorizationHandler<StudentRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, StudentRequirement requirement)
        {
            if (context.User == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var identities = context.User.Identities
                .Where(i => i.HasClaim(c => c.Type == "PersonalDataFilled"));
            if (!identities.Any())
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var filled = bool.Parse(
                identities.Select(c => c.FindFirst("PersonalDataFilled")).FirstOrDefault().Value);

            if (filled)
                context.Succeed(requirement);
            else
            {
                context.Fail();
            }
                
            return Task.CompletedTask;
        }
    }
}
