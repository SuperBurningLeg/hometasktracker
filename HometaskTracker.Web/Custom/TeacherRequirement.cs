﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Authorization;

namespace HometaskTracker.Web.Custom
{
    public class TeacherRequirement : AuthorizationHandler<TeacherRequirement>, IAuthorizationRequirement
    {
        private readonly EfDbContext _context;
        public TeacherRequirement()
        {
            //_context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TeacherRequirement requirement)
        {
            if (context.User == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var identities = context.User.Identities
    .Where(i => i.HasClaim(c => c.Type == "IsTeacher"));
            if (!identities.Any())
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var isTeacher =bool.Parse(
                identities
                    .Select(i => i.FindFirst(c => c.Type == "IsTeacher")).FirstOrDefault()?.Value);

            if (isTeacher)
                context.Succeed(requirement);
            else
                context.Fail();

            return Task.CompletedTask;

        }
    }
}
