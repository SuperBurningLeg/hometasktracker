#if DEBUG
#define CREATEATABASE
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HometaskTracker.Web
{
    static class DatabaseSeedExtensions
    {
        public static void SeedData(this IApplicationBuilder builder)
        {
#if CREATEATABASE
            using (var serviceScope = builder.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<EfDbContext>())
                {
                    context.Database.EnsureDeleted();
                    context.Database.Migrate();
                    AddUsers(context);
                    AddSubjects(context);
                    AddHometasks(context);
                    AddStudentSubjects(context);
                    AddCommits(context);
                    AddMarks(context);

                }
            }
#endif

        }

        private static void AddMarks(EfDbContext context)
        {
            const int part = 4;
            var random = new Random();
            var repo = new CommitRepository(context, new SubjectService(context));
            foreach (var commit in repo.GetLastCommits())
            {

                var rNumber = random.Next(part);
                if(rNumber % part != 0)
                    repo.AddMark(commit.Commit.Id, commit.Student.Id, commit.Hometask.Id,
                        random.Next(0, commit.Hometask.MaxMark), "").GetAwaiter().GetResult();
            }
        }

        private static void AddCommits(EfDbContext context)
        {
            const int commitsCount = 3;
            Random r = new Random();
            var changes = new List<CommitChange>();
            var hometasks = context.Hometasks.Include(h => h.Subject)
                .ThenInclude(s => s.StudentSubjects).ThenInclude(ss => ss.Student).Where(h => h.Subject.SubjectName == "Web Programming").ToList();
            foreach (var user in context.Users.Where(u => u.UserName != "kostya9").ToList())
            {
                for (int i = 0; i < commitsCount; i++)
                {
                    var commit = new Commit
                    {
                        TimeStamp = GetRandomDate(r)
                    };
                    context.Commits.Add(commit);
                    context.SaveChanges();
                    var index = r.Next(hometasks.Count);
                    var hometask = hometasks.ElementAt(index);
                    var repoName =
                        hometask.Subject.StudentSubjects.FirstOrDefault(s => s.StudentId == user.Id).RepositoryName;
                    var change = new CommitChange
                    {
                        CommitId = commit.Id,
                        Path = (repoName.IsNullOrEmpty() ? "" : repoName) + 
                            hometask.RepositoryPath + "/" + r.Next(100).ToString(),
                        StudentId = user.Id,
                        SubjectId = hometask.SubjectId,
                    };
                    changes.Add(change);
                }
            }
            context.AddRange(changes);
            context.SaveChanges();
        }

        private static DateTime GetRandomDate(Random r)
        {
            return new DateTime(2016, r.Next(12) + 1, r.Next(28) + 1);
        }

        private static void AddHometasks(EfDbContext context)
        {
            Random r = new Random();
            var subjectId = GetSubjectIdByName(context, "Physics");
            var physicsHometasks = new List<Hometask>
            {
                new Hometask
                {
                    Name = "Optics",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Mechanics",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Electricity",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Gaussian Theorem",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                }
            };
            subjectId = GetSubjectIdByName(context, "Maths");
            var mathsHometasks = new List<Hometask>
            {
                new Hometask
                {
                    Name = "Algebra",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Geometry",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 5,
                    RepositoryPath = "/"
                }
            };
            subjectId = GetSubjectIdByName(context, "Web Programming");
            var webProgrammingHometasks = new List<Hometask>
            {
                new Hometask
                {
                    Name = "Javascript",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 8,
                    RepositoryPath = "/webprogramming/javascript"
                },
                new Hometask
                {
                    Name = "Python",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 3,
                    RepositoryPath = "/webprogramming/python"
                },
                new Hometask
                {
                    Name = "NodeJs intro",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 4,
                    RepositoryPath = "/webprogramming/nodejs"
                },
                new Hometask
                {
                    Name = "Html Forms+Node",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 6,
                    RepositoryPath = "/webprogramming/forms"
                }
                ,
                new Hometask
                {
                    Name = "Deploying",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 4,
                    RepositoryPath = "/webprogramming/deploy"
                }
            };
            subjectId = GetSubjectIdByName(context, "Object Oriented Programming");
            var objectOrientedProgrammingHometasks = new List<Hometask>
            {
                new Hometask
                {
                    Name = "C#",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 6,
                    RepositoryPath = "/oop/csharp"
                },
                new Hometask
                {
                    Name = "Arithmetic operations",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 2,
                    RepositoryPath = "/oop/arithm"
                },
                new Hometask
                {
                    Name = "Classes and Structs",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 3,
                    RepositoryPath = "/oop/classessturcts"
                },
                new Hometask
                {
                    Name = "Advanced C#",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 10,
                    RepositoryPath = "/oop/advancedcsharp"
                },
                new Hometask
                {
                    Name = "Mr Fowler",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 10,
                    RepositoryPath = "/oop/fowler"
                }
            };
            subjectId = GetSubjectIdByName(context, "Operation Systems");
            var operationSystems = new List<Hometask>
            {
                new Hometask
                {
                    Name = "Windows",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 10,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Linux",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 2,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Else",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 1,
                    RepositoryPath = "/"
                }
            };
            subjectId = GetSubjectIdByName(context, "Algorithms and Data Structures");
            var algorithmsHometasks = new List<Hometask>
            {
                new Hometask
                {
                    Name = "Sort",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 20,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Trees",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 10,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Heaps",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 6,
                    RepositoryPath = "/"
                },
                new Hometask
                {
                    Name = "Else",
                    SubjectId = subjectId,
                    Deadline = GetRandomDate(r),
                    MaxMark = 2,
                    RepositoryPath = "/"
                }
            };
            context.AddRange(physicsHometasks);
            context.SaveChanges();
            context.AddRange(mathsHometasks);
            context.SaveChanges();
            context.AddRange(webProgrammingHometasks);
            context.SaveChanges();
            context.AddRange(objectOrientedProgrammingHometasks);
            context.SaveChanges();
            context.AddRange(algorithmsHometasks);
            context.SaveChanges();
            context.AddRange(operationSystems);
            context.SaveChanges();
        }

        private static void AddStudentSubjects(EfDbContext context)
        {
            var subscriptions = new List<StudentSubject>
            {

            };
            var random = new Random();
            var randomSubjects = new string[]
            {
                "Maths", "Physics", "Operation Systems", "Algorithms and Data Structures"
            };
            subscriptions.Add(
                    new StudentSubject
                    {
                        StudentId = GetUserIdByName(context, "kostya9"),
                        SubjectId = GetSubjectIdByName(context, "Physics"),
                        SubscriptionStatus = SubscriptionStatus.Accepted,
                        RepositoryName = ""
                    });
            subscriptions.Add(
        new StudentSubject
        {
            StudentId = GetUserIdByName(context, "kostya9"),
            SubjectId = GetSubjectIdByName(context, "Web Programming"),
            SubscriptionStatus = SubscriptionStatus.Accepted,
            RepositoryName = ""
        });
            foreach (var user in context.Users.Where(u => u.UserName != "kostya9").ToList())
            {
                var id = random.Next(randomSubjects.Length);
                subscriptions.Add(
                    new StudentSubject
                    {
                        StudentId = user.Id,
                        SubjectId = GetSubjectIdByName(context, randomSubjects[id]),
                        SubscriptionStatus = SubscriptionStatus.Accepted,
                        RepositoryName = ""
                    });
                var nextId = random.Next(randomSubjects.Length);
                if(id != nextId)
                    subscriptions.Add(
                        new StudentSubject
                        {
                            StudentId = user.Id,
                            SubjectId = GetSubjectIdByName(context, randomSubjects[nextId]),
                            SubscriptionStatus = SubscriptionStatus.Pending,
                            RepositoryName = ""
                        });
                subscriptions.Add(new StudentSubject
                {
                    StudentId = user.Id,
                    SubjectId = GetSubjectIdByName(context, "Web Programming"),
                    SubscriptionStatus = SubscriptionStatus.Accepted,
                    RepositoryName = ""
                });
                subscriptions.Add(new StudentSubject
                {
                    StudentId = user.Id,
                    SubjectId = GetSubjectIdByName(context, "Object Oriented Programming"),
                    SubscriptionStatus = SubscriptionStatus.Accepted,
                    RepositoryName = ""
                });
            }
            context.AddRange(subscriptions);
            context.SaveChanges();
        }

        private static User FindByName(EfDbContext context, string name)
        {
            return context.Users.FirstOrDefault(u => u.UserName == name);
        }

        private static string GetUserIdByName(EfDbContext context,
            string name)
        {
            return FindByName(context, name).Id;
        }

        private static int GetSubjectIdByName(EfDbContext context,
            string name)
        {
            return context.Subjects.FirstOrDefault(s => s.SubjectName == name).SubjectId;
        }

        private static void AddSubjects(EfDbContext context)
        {
            var mainId = GetUserIdByName(context, "kostya9");
            var subjects = new List<Subject>()
            {
                new Subject("Maths") {TeacherId = mainId},
                new Subject("Physics"){TeacherId = mainId},
                new Subject("Web programming"){TeacherId = mainId},
                new Subject("Object Oriented Programming"){TeacherId = mainId},
                new Subject("Operation Systems"){TeacherId = mainId},
                new Subject("Algorithms and Data Structures")
            };
            context.AddRange(subjects);
            context.SaveChanges();
        }

        private static void AddUsers(EfDbContext context)
        {
            var users = new List<User>
            {
                // Me
                new User
                {
                    UserName = "kostya9",
                    IsTeacher = true,
                    Name = "Kostya Shar",
                    Email = "kostya9@gmail.com",
                    NormalizedEmail = "KOSTYA9@GMAIL.COM"
                },
                new User
                {
                    UserName = "someone0",
                    IsTeacher = false,
                    Name = "Barclay Deimos",
                },
                new User
                {
                    UserName = "someone1",
                    IsTeacher = false,
                    Name = "Gotthold Darien",
                },
                new User()
                {
                    UserName = "someone2",
                    IsTeacher = false,
                    Name = "Livio Olaf"
                },
                new User
                {
                    UserName = "someone3",
                    IsTeacher = false,
                    Name = "Eusebios Pekko",
                },
                new User
                {
                    UserName = "someone4",
                    IsTeacher = false,
                    Name = "Mars Finnr",
                },
                new User
                {
                    UserName = "someone5",
                    IsTeacher = false,
                    Name = "Vulferam �thelred",
                }
            };
            context.AddRange(users);
            context.SaveChanges();
        }

    }
}