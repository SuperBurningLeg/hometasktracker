﻿/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({
        copy : {
            main: {
                files: [
                    {
                        cwd: "../HometaskTracker.Front/**",
                        src: "**/*",
                        dest: "wwwroot/",
                        expand: true
                    }
                ]
            }
        }
    });
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.registerTask("all", ["copy"]);
    grunt.registerTask("default", "all");
};