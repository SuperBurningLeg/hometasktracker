﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HometaskTracker.Web.Migrations
{
    public partial class AddHometasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Hometasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Deadline = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<int>(nullable: false),
                    RepositoryPath = table.Column<string>(nullable: true),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hometasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hometasks_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AddColumn<int>(
                name: "HometaskId",
                table: "Marks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Marks_HometaskId",
                table: "Marks",
                column: "HometaskId");

            migrationBuilder.CreateIndex(
                name: "IX_Hometasks_SubjectId",
                table: "Hometasks",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Marks_Hometasks_HometaskId",
                table: "Marks",
                column: "HometaskId",
                principalTable: "Hometasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Marks_Hometasks_HometaskId",
                table: "Marks");

            migrationBuilder.DropIndex(
                name: "IX_Marks_HometaskId",
                table: "Marks");

            migrationBuilder.DropColumn(
                name: "HometaskId",
                table: "Marks");

            migrationBuilder.DropTable(
                name: "Hometasks");
        }
    }
}
