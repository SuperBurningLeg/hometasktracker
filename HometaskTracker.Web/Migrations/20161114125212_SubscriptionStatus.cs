﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using HometaskTracker.Domain.Entities;

namespace HometaskTracker.Web.Migrations
{
    public partial class SubscriptionStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubscriptionStatus",
                table: "StudentSubject",
                nullable: false,
                defaultValue: HometaskTracker.Domain.Entities.SubscriptionStatus.Pending);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubscriptionStatus",
                table: "StudentSubject");
        }
    }
}
