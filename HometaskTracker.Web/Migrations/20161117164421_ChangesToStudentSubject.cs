﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HometaskTracker.Web.Migrations
{
    public partial class ChangesToStudentSubject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commits_StudentSubject_StudentId_SubjectId",
                table: "Commits");

            migrationBuilder.DropIndex(
                name: "IX_Commits_StudentId_SubjectId",
                table: "Commits");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Commits");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Commits");

            migrationBuilder.AddColumn<string>(
                name: "StudentId",
                table: "Changes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Changes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Changes_StudentId_SubjectId",
                table: "Changes",
                columns: new[] { "StudentId", "SubjectId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Changes_StudentSubject_StudentId_SubjectId",
                table: "Changes",
                columns: new[] { "StudentId", "SubjectId" },
                principalTable: "StudentSubject",
                principalColumns: new[] { "StudentId", "SubjectId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Changes_StudentSubject_StudentId_SubjectId",
                table: "Changes");

            migrationBuilder.DropIndex(
                name: "IX_Changes_StudentId_SubjectId",
                table: "Changes");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Changes");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Changes");

            migrationBuilder.AddColumn<string>(
                name: "StudentId",
                table: "Commits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Commits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Commits_StudentId_SubjectId",
                table: "Commits",
                columns: new[] { "StudentId", "SubjectId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Commits_StudentSubject_StudentId_SubjectId",
                table: "Commits",
                columns: new[] { "StudentId", "SubjectId" },
                principalTable: "StudentSubject",
                principalColumns: new[] { "StudentId", "SubjectId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
