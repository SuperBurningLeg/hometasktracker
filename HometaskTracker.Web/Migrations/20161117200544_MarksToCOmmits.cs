﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HometaskTracker.Web.Migrations
{
    public partial class MarksToCOmmits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CommitId",
                table: "Marks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Marks_CommitId",
                table: "Marks",
                column: "CommitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Marks_Commits_CommitId",
                table: "Marks",
                column: "CommitId",
                principalTable: "Commits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Marks_Commits_CommitId",
                table: "Marks");

            migrationBuilder.DropIndex(
                name: "IX_Marks_CommitId",
                table: "Marks");

            migrationBuilder.DropColumn(
                name: "CommitId",
                table: "Marks");
        }
    }
}
