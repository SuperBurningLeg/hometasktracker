﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HometaskTracker.Web.Migrations
{
    public partial class UniqueIndexMarkBug : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Marks_CommitId_HometaskId_StudentId",
                table: "Marks");

            migrationBuilder.CreateIndex(
                name: "IX_Marks_CommitId_HometaskId_StudentId",
                table: "Marks",
                columns: new[] { "CommitId", "HometaskId", "StudentId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Marks_CommitId_HometaskId_StudentId",
                table: "Marks");

            migrationBuilder.CreateIndex(
                name: "IX_Marks_CommitId_HometaskId_StudentId",
                table: "Marks",
                columns: new[] { "CommitId", "HometaskId", "StudentId" });
        }
    }
}
