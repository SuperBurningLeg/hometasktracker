﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HometaskTracker.Web.Models
{
    public class GithubPushEvent
    {
        public List<GithubCommit> Commits { get; set; }
        public Repository Repository;
    }

    public class Repository
    {
        public string Name { get; set; }
        public Owner Owner { get; set; }
    }

    public class Owner
    {
        public string Name { get; set; }
    }

    public class GithubCommit
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<string> Added;
        public List<string> Removed;
        public List<string> Modified;
    }
}
