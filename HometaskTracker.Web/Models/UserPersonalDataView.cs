﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http; 

namespace HometaskTracker.Web.Models
{
    public class UserPersonalDataView
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public IFormFile Avatar { get; set; }
    }
}
