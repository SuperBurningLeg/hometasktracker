using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using AspNet.Security.OAuth.GitHub;
using HometaskTracker.Domain;
using HometaskTracker.Domain.Abstract;
using HometaskTracker.Domain.Concrete;
using HometaskTracker.Domain.Entities;
using HometaskTracker.Web.Areas.Api.Models;
using HometaskTracker.Web.Custom;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using SendGrid;

namespace HometaskTracker.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
#if DEBUG
                .AddJsonFile("appsettings.json", true, true)
#endif
#if RELEASE
                .AddEnvironmentVariables("COREAPP_")
#endif
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAntiforgery(options => { options.HeaderName = "X-XSRF-TOKEN"; });
            // Add framework services.
            AddDbContext(services);
            AddIdentityAuthentication(services);

            //services.AddScoped<IUsersRepository, EfDbUsersRepository>();
            //services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();
            AddFactoriesToDi(services);
            services.AddLogging();

            services.AddSession();
            services.AddDataProtection();
            services.Configure<SendGridOptions>(Configuration);

            services.AddMvc();
            services.AddMvc()
                .AddJsonOptions(opt => { opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore; });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("HasPersonal", pol => pol.RequireClaim("Name"));
                options.AddPolicy("Teacher", builder => builder.AddRequirements(new TeacherRequirement()));
                options.AddPolicy("Student", builder => builder.AddRequirements(new StudentRequirement()));
            });
            services.AddSingleton<IAuthorizationHandler, StudentRequirementHandler>();
        }

        private void AddDbContext(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<EfDbContext>((provider,
                        options) =>
                        options.UseSqlServer(Configuration.GetValue<string>("ConnectionString"),
                                b => b.MigrationsAssembly("HometaskTracker.Web"))
                            .UseInternalServiceProvider(provider));
        }

        private static void AddIdentityAuthentication(IServiceCollection services)
        {
            services.AddScoped<IdentityDbContext<User>, EfDbContext>();
            services.AddScoped<IUserClaimsPrincipalFactory<User>, AppClaimsPrincipleFactory>();
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<EfDbContext>()
                .AddUserStore<AppUserStore>()
                .AddUserManager<AppUserManager>()
                .AddDefaultTokenProviders();
            services.AddScoped<TeacherInviter>();
        }

        private static void AddFactoriesToDi(IServiceCollection services)
        {
            services.AddScoped<ISubjectService, SubjectService>();
            services.AddScoped<ISubjectSubscriber, EfDbSubjectSubscriber>();
            services.AddScoped<SubscribedSubjectsRepository>();
            services.AddScoped<CommitRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IAntiforgery antiforgery)
        {
            // Authentication
            app.UseSession();
            app.UseIdentity();
            app.SetGithubAuthentication(Configuration.GetValue<string>("GithubClientId"),
                Configuration.GetValue<string>("GithubClientSecret"));
            app.UseApiAuthentication(antiforgery);
            app.SetCsrfTokensForApiUsage(antiforgery);
            SetLogging(app, env, loggerFactory);
            app.UseAngularStaticFiles();
            app.UseMvcWithDefaultRoute();
            app.SeedData();
            // Routes
            app.SetIgnoreAngularRoutes();
            app.SetMvcRoutes();
            app.SeedData();
        }

        private void SetLogging(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug(LogLevel.Trace);
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                loggerFactory
                    .AddConsole()
                    .AddDebug();
                app.UseExceptionHandler("/Home/Error");
            }
        }
    }

    #region Extension Methods

    internal static class AppBuilderExtensions
    {
        public static void SetCsrfTokensForApiUsage(this IApplicationBuilder app,
    IAntiforgery antiforgery)
        {
            app.Use((context,
                    next) =>
            {
                if (!Path.HasExtension(context.Request.Path) &&
                    (context.Request.Path.StartsWithSegments("/angularapp/teacher") ||
                     context.Request.Path.StartsWithSegments("/angularapp/student")))
                {
                    var tokens = antiforgery.GetAndStoreTokens(context);
                    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
                        new CookieOptions { HttpOnly = false });
                }

                return next();
            });
        }
        public static void UseApiAuthentication(this IApplicationBuilder app,
            IAntiforgery antiforgery)
        {
            app.Use(async (context,
                    next) =>
                {
                    var query = context.Request.Query;
                    if (!query.ContainsKey("user_name") || !query.ContainsKey("secret"))
                    {
                        await next();
                        return;
                    }
                    var failed = true;
                    var userManager = app.ApplicationServices.GetService<UserManager<User>>();
                    StringValues userName;
                    query.TryGetValue("user_name", out userName);
                    var user = await userManager.FindByNameAsync(userName.FirstOrDefault());

                    if (user != null)
                    {
                        StringValues secret;
                        query.TryGetValue("secret", out secret);
                        using (var serviceScope = app.ApplicationServices
                            .GetRequiredService<IServiceScopeFactory>()
                            .CreateScope())
                        {
                            var signinManager = serviceScope.ServiceProvider.GetService<SignInManager<User>>();
                            if (user.ApiSecret == secret.FirstOrDefault())
                            {
                                context.User = await signinManager.CreateUserPrincipalAsync(user);

                                var tokens = antiforgery.GetAndStoreTokens(context);
                                var token = tokens.RequestToken;
                                context.Request.Headers.Add(new KeyValuePair<string, StringValues>("X-XSRF-TOKEN",
                                    token));
                                failed = false;
                            }
                        }
                    }

                    if (failed)
                    {
                        context.User = new ClaimsPrincipal();
                        context.Response.StatusCode = 401;
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorInformation
                        {
                            Code = 401,
                            Message = "Could not log in with that secret and username"
                        }));
                        return;
                    }
                    await next();
                });
        }

        public static void SetGithubAuthentication(this IApplicationBuilder app,
            string clientId,
            string clientSecret)
        {
            var options = new GitHubAuthenticationOptions
            {
                SaveTokens = true,
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                ClientId = clientId,
                ClientSecret = clientSecret,
                CallbackPath = new PathString("/signin-github"),
                Scope = {"user:email", "write:repo_hook"}
            };

            app.UseGitHubAuthentication(options);
        }

        public static void UseAngularStaticFiles(this IApplicationBuilder app)
        {
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
#if DEBUG
                    Path.Combine(Directory.GetCurrentDirectory(), @"../HometaskTracker.Front")),
#endif
#if RELEASE
                Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/HometaskTracker.Front")),
#endif
                RequestPath = new PathString("/angularapp")
            });
        }

        public static void SetIgnoreAngularRoutes(this IApplicationBuilder app)
        {
            app.Use(async (context,
                    next) =>
                {
                    await next();

                    if ((context.Response.StatusCode == 404) && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Response.StatusCode = 200;
                        var path = context.Request.Path;
                        if (path.StartsWithSegments(new PathString("/angular/app")))
                            context.Request.Path = "/angularapp/app";
                        else if (path.StartsWithSegments("/angularapp/teacher"))
                            context.Request.Path = "/angularapp/teacher";
                        else if (path.StartsWithSegments("/angularapp/student"))
                            context.Request.Path = "/angularapp/student";
                        else
                            context.Response.StatusCode = 404;
                        await next();
                    }
                });
        }

        public static void SetMvcRoutes(this IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "github",
                    "payload", new {controller = "Github", action = "Payload"});

                routes.MapRoute(
                    "angular",
                    "angularapp/app", new {controller = "Home", action = "AngularApp"});

                routes.MapRoute(
                    "teacher",
                    "angularapp/teacher", new {controller = "Home", action = "TeacherApp"});

                routes.MapRoute(
                    "student",
                    "angularapp/student", new {controller = "Home", action = "StudentApp"});

                routes.MapRoute(
                    "id",
                    "{area:exists}/{controller}/{action}/{id}");

                routes.MapRoute(
                    "areaRoute",
                    "{area:exists}/{controller=Tests}/{action=Test}");

                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}");
            });
        }
    }

    #endregion
}
